<?php
require_once __DIR__ . '/RewriteSQL.php';
require_once __DIR__ . '/../../art/classes.php';

use PHPUnit\Framework\TestCase;

class RewriteSQLTest extends TestCase{
    
    static $stepperFile = '/opt/bitnami/apache2/htdocs/wp-content/pg4wp/testingSets/';
    static $incollectMode;


    public static function setUpBeforeClass() : void {
        
        //to update datasets
        //self::$incollectMode = (new CreateDataset())->incollectMode;
        //exit();

        
    }

    
    

    /**
     * @dataProvider provider_testRw_SpecialCases
     */
    public function testRw_SpecialCases(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
        
    }
    
    /**
     * provider for testRw_SpecialCases
     */
    public function provider_testRw_SpecialCases(){
        
        return $this->dataParser(__METHOD__);
        
    }
    
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_SpecialCases_1
     */
    public function testRw_SpecialCases_1(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
        
    }
    
    public function provider_testRw_SpecialCases_1(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_SpecialCases_2
     */
    public function testRw_SpecialCases_2(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }
    
    public function provider_testRw_SpecialCases_2(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_SpecialCases_3
     */
    public function testRw_SpecialCases_3(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_SpecialCases_3(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_YEAR
     */
    public function testRw_YEAR(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_YEAR(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_DATE_FORMAT
     */
    public function testRw_DATE_FORMAT(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_DATE_FORMAT(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_AVG
     */
    public function testRw_AVG(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_AVG(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_setExplicitComparing
     */
    public function testRw_setExplicitComparing(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_setExplicitComparing(){
        
        return $this->dataParser(__METHOD__);
        
    }
    /**
     * @dataProvider provider_testRw_DataConversion
     */
    ###################################################################
    #NEXT
    ###################################################################
    public function testRw_DataConversion(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_DataConversion(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_ChangeColumnNameCase
     */
    public function testRw_ChangeColumnNameCase(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_ChangeColumnNameCase(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_SUM
     */
    public function testRw_SUM(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_SUM(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_EXTRACT
     */
    public function testRw_EXTRACT(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_EXTRACT(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_UNSIGNED_IN_OBY
     */
    public function testRw_UNSIGNED_IN_OBY(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_UNSIGNED_IN_OBY(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_Radians_to_explicit_type
     */
    public function testRw_Radians_to_explicit_type(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_Radians_to_explicit_type(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_Solve_select_list_error
     */
    public function testRw_Solve_select_list_error(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_Solve_select_list_error(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_Solve_GROUP_BY_error
     */
    public function testRw_Solve_GROUP_BY_error(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }

    public function provider_testRw_Solve_GROUP_BY_error(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_HAVING
     */
    public function testRw_HAVING(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }
    
    public function provider_testRw_HAVING(){
        
        return $this->dataParser(__METHOD__);
        
    }
    ###################################################################
    #NEXT
    ###################################################################
    /**
     * @dataProvider provider_testRw_Comparing
     */
    public function testRw_Comparing(bool $modified, string $incoming, string $rewrited){
        
        if (self::$incollectMode){
            $this->markTestSkipped();return;
        }
        
        $result  = $this->runTesting(__METHOD__,$incoming);
        
        if ($modified){
            
            $this->assertEquals($rewrited,$result,"Query did not match rewrited string!");
            
        } else {
            
            $this->assertEquals($rewrited,$result,"Query did not match incoming string!");
            
        }
    }
    
    public function provider_testRw_Comparing(){
        
        return $this->dataParser(__METHOD__);
        
    }
    
    /**
     * 
     * @param string $testMethod - name of the method where it is called from
     * @param string $incoming - sql to modify
     * @return type rewrited string
     */
    public function runTesting(string $testMethod,string $incoming) {
        
        $meth_name = lcfirst( str_replace(__CLASS__ . '::test', '', $testMethod));
        
        $rewrite    = new RewriteSQL($incoming, '');
        
        $reflection = new ReflectionClass('RewriteSQL');
        
        $method     = $reflection->getMethod($meth_name);

        $method->setAccessible(true);

        $method->invoke($rewrite);

        return trim($rewrite->postgres);
        
    }
    /**
     * 
     * parsing dataset and return array of results
     * 
     * 
     * @return type array
     */
    public function dataParser($method){
        
        $filename = lcfirst (str_replace(__CLASS__.'::provider_test', '', $method));
        
        $file = file_get_contents(self::$stepperFile . $filename . '.set');
        
        $pattern_inc = '/----SB-((?:NOT)?MODIF)-{3,4}\n(.*?)\n----SA----/s';
        $pattern_rew = '/----SA----\n(.*?)\n----E/s';
        
        preg_match_all($pattern_inc, $file, $matches_inc);
        preg_match_all($pattern_rew, $file, $matches_rew);
        
        unset($file);
        
        if (count($matches_inc[0]) != count($matches_rew[0]) ){
            
            throw new \Exception("Dataset strucure corrupted!  $filename.set");
            
        }
        
        $ifModified_array = $matches_inc[1];
        
        $incoming_array = $matches_inc[2];
        
        $rewrited_array = $matches_rew[1];
        
        $results = [];
        
        foreach ($ifModified_array as $key => $value) {
            
            $results[] = [
                $value == 'NOTMODIF' ? FALSE : TRUE,
                $incoming_array[$key],
                $rewrited_array[$key]
            ];
            
        }
        
        unset($matches_inc);
        unset($matches_rew);
        
        
        unset($incoming_array);
        unset($rewrited_array);
        
        return $results;
        
    }
    
    
}

/**
 * 
 * 
 */
class CreateDataset{
    
    public $incollectMode = true;
    
    public $methods = [
            'rw_SpecialCases',
            'rw_SpecialCases_1',
            'rw_SpecialCases_2',
            'rw_SpecialCases_3',
            'rw_YEAR',
            'rw_DATE_FORMAT',
            'rw_AVG',
            'rw_setExplicitComparing',
            'rw_DataConversion',
            'rw_ChangeColumnNameCase',
            'rw_SUM',
            'rw_EXTRACT',
            'rw_UNSIGNED_IN_OBY',
            'rw_Radians_to_explicit_type',
            'rw_Solve_select_list_error',
            'rw_Solve_GROUP_BY_error',
            'rw_HAVING',
            'rw_Comparing',
            
        ];
    
    public function __construct() {
        
        $this->incollectMode = true;
        
        //clear datasets
        foreach ($this->methods as  $meth_name) {
            
            //truncate
            if(file_put_contents(RewriteSQLTest::$stepperFile . $meth_name . '.set', '') === false){

                throw new Error("Couldn't write into $meth_name.set" );

            }
            
        }
        
        $dataset = $this->dataProvider();
        
        foreach ($dataset as  $value) {
            
            $this->CreateTestingSets($value[0], $value[1], $value[2]);
            
        }
    }
    ####################################################################################
    #FOLOWING METHOD ONLY FOR NEW TESTING SETS
    #DO NOT UNCOMMENT IT IF YOU DONT NEED TO OVERRIDE /opt/bitnami/apache2/htdocs/wp-content/pg4wp/logs/tesetingSets
    ####################################################################################
    
    /**
     * this method will update results dataset
     * need to lounch it once, before modification of RewriteSQL
     * BEFORE YOU UNCOMMENT THIS PART, PLEASE UNCOMMENT - self::seUpBeforeClass()
     * 
     * @dataProvider dataProvider
     */
    public function CreateTestingSets($sql,$incoming,$rewrited) {
        
        $reflection = new ReflectionClass('RewriteSQL');
        
        $rewrite    = new RewriteSQL($incoming, $sql);
        
        foreach ($this->methods as  $meth_name) {
            
            $method     = $reflection->getMethod($meth_name);

            $method->setAccessible(true);

            $before_rw  = trim($rewrite->postgres);

            $method->invoke($rewrite);

            $after_rw   =  trim($rewrite->postgres);

            if ($before_rw != $after_rw){
                
                //modified
                if(file_put_contents(RewriteSQLTest::$stepperFile . $meth_name . '.set', "\n----SB-MODIF----\n$before_rw\n----SA----\n$after_rw\n----E----\n",FILE_APPEND) === false){

                    throw new Error("Couldn't write into $method.set" );

                }

            } else {
                
                //NOT modified
                if(file_put_contents(RewriteSQLTest::$stepperFile . $meth_name . '.set', "\n----SB-NOTMODIF----\n$before_rw\n----SA----\n$after_rw\n----E----\n",FILE_APPEND) === false){

                    throw new Error("Couldn't write into $method.set" );

                }
                
            }
        }
        
    }
    
    /**
     * ONLY FOR NEW TESTING SETS in testingSets
     * 
     * parsing RewriteSQL.log and return array of sql string
     * 
     * to fillup RewriteSQL.log - need to uncomment RewriteSQL call in  driver_pgsql.php:
     * 
     * 'SHOW_CHANGES' => 1,
     * 'REPORT_LEVEL' => 1  
     * 
     * then you can work with webinterface and collect all modified queries.
     * when you get enough data go to driver_pgsql.php and comment those two lines
     * and work with this class
     * 
     * @return type array
     */
    public function dataProvider(){
        
        $file = file_get_contents(__DIR__ . '/logs/RewriteSQL.log');
        
        $pattern_sql = '/SQL:\n(.*?)\n-----------------SQL END/s';
        $pattern_inc = '/INCOMMING:\n(.*?)\n-----------------INCOMMING END/s';
        $pattern_rew = '/Rewrited:\n(.*?)\n-----------------REWRITED END/s';
        
        preg_match_all($pattern_sql, $file, $matches_sql);
        preg_match_all($pattern_inc, $file, $matches_inc);
        preg_match_all($pattern_rew, $file, $matches_rew);
        
        if (count($matches_inc[1]) != count($matches_sql[1]) 
                || count($matches_inc[1]) != count($matches_rew[1]) ){
            
            exit('LOG FILE STRUCTURE CoRRuPTED');
        }
        
        $sql_array = $matches_sql[1];
        
        $incoming_array = $matches_inc[1];
        
        $rewrited_array = $matches_rew[1];
        
        $results = [];
        
        $sql_array_unique = array_unique($sql_array);
        
        unset($sql_array);
        
        foreach ($sql_array_unique as $key => $value) {
            
            $results[] = [
                $value,
                $incoming_array[$key],
                $rewrited_array[$key]
            ];
            
        }
        
        unset($file);
        
        unset($matches_sql);
        unset($matches_inc);
        unset($matches_rew);
        
        unset($sql_array_unique);
        unset($incoming_array);
        unset($rewrited_array);
        
        return $results;
        
    }
    
}