<?php


/**
 * Description of RewriteSQL:
 * working with sql query string rewriting it.
 * 
 * 
 * 
 * @author Arthur
 */
class RewriteSQL {
    
    
    private $logFile = __DIR__ . '/logs/RewriteSQL.log';
    
    public  $incoming;
    
    public  $postgres;
    
    private  $show_changes = false;
    
    private  $report_level = 0;
    
    private  $initial = false;
    
    private  $qurrentMessage = '';
    
    private  $select_group = '';
    
    private  $update_group = '';
    
    private  $query_type = '';
    
    private  $ignore_types = ['UPDATE','SET_SESSION','INSERT'];
    
    private  $orderby_group = '';
    
    private  $limit_group = '';

    /**
     * 
     * @param string $sql - modified sql from pg4wp_rewrite
     * @param string $initial - initial sql which pg4wp_rewrite takes
     * @param array $settings setting 
     *              to use assosiative array
     * [
     *  'SHOW_CHANGES' = (int) 1 = store in log file, 
     *                         2 = show on page
     * 
     *  'REPORT_LEVEL' = (int) 1 = display initial incoming queries and final outgoing queries
     *                         2 = display incoming and outgoing queries after each modification
     * ]
     * 
     * 
     * @throws ErrorException
     */
    public function __construct(string $sql, string $initial, array $settings = []) {
        
        
        if (empty($sql))    
            
            throw new ErrorException('Invalid sql string');

        $this->incoming = trim($sql);

        $this->postgres = $this->incoming;
        
        $this->initial = $initial;
        
        if (!empty($settings)){
            
            $this->addSettings($settings);
            
        }
        
    }
    /**
     * 
     * this settings should be provided 
     * when the object is initialized
     * description of the settings you can find in __contruct description
     * 
     * @param type array $settings settings that should be used by class
     */
    private function addSettings($settings) {
        
        foreach ($settings as $setting => $value) {
            
            if ($setting == 'SHOW_CHANGES'){
                
                $this->show_changes = $value;
                
                
            }
            
            if ($setting == 'REPORT_LEVEL'){
                
                $this->report_level = $value;
                
                
            }
        }
    }
    /**
     * displays query before and after changes
     * same query will be launched each time
     * if 
     */
    private function showChanges(int $type,$detailed = false) {
        
        //if show mode is enabled
        if (!$this->show_changes ) return;
        
        //if one time reminder
        if ($this->report_level !== 2 
                && $detailed === false) return;
        
        switch($type){
            case 0 : $this->qurrentMessage = "SQL:\n" . $this->initial . "\n-----------------SQL END-----------------\n";            break;
            case 1 : $message = $this->qurrentMessage . "INCOMMING:\n$this->incoming\n-----------------INCOMMING END-----------------\nRewrited:\n" . $this->postgres . "\n-----------------REWRITED END-----------------\n\n\n\n";            break;
        }
        
        //if nothing is modified
        if ($this->postgres == $this->incoming) return;
            
        if ($this->show_changes === 1){
            
            file_put_contents($this->logFile, $message,FILE_APPEND);
            
        }
        
        if ($this->show_changes === 2){
            
            echo '<pre class="rewrite_output">';
                var_dump($message);
            echo '</pre>';
            
        }
        
        
        
    }
    /**
     * run all tests and reqrite suntax one by one
     */
    private function toPostgres() {
        
        if (in_array($this->getQueryType(), $this->ignore_types))
                return;
        
        $this->pars_query();
        
        //replace special cases FIRST
        $this->rw_SpecialCases();
        
        $this->rw_SpecialCases_1();
        
        $this->rw_SpecialCases_2();
        
        $this->rw_SpecialCases_3();

        //replace sum call
        $this->rw_YEAR();
        
        //DATE_FORMAT
        $this->rw_DATE_FORMAT();

        //AVG
        $this->rw_AVG();

        //set data type in comparing string
        $this->rw_setExplicitComparing();

        //conver string from val+0  to val::int
        $this->rw_DataConversion();

        //to lower case colum names
        $this->rw_ChangeColumnNameCase();
        
        //replace sum call
        $this->rw_SUM();
        
        //replace EXTRACT
        $this->rw_EXTRACT();
        
        //replace ORDER BY ... UNSIGN
        $this->rw_UNSIGNED_IN_OBY();
        
        //set explicit type for redians function
        $this->rw_Radians_to_explicit_type();
        
        //resolve error with select list
        $this->rw_Solve_select_list_error();
        
        //resolve error with select list
        $this->rw_Solve_GROUP_BY_error();
        
        //replace HAVING cause
        $this->rw_HAVING();
        
        //set explicit comparing for case a<>b
        $this->rw_Comparing();
    }
    /**
     * Initiate syntax rewrite 
     * and return string in postgres syntax
     * 
     * @return type - string postgres query
     */
    public function getPostgres() {
        
        //display changes
        $this->showChanges(0,TRUE);


        $this->toPostgres();
        
        //display changes
        $this->showChanges(1,TRUE);
        
        return $this->postgres;
        

    }
    private function pars_query() {
        
        
        $sql = $this->postgres;
        
        //get LIMIT
        $limit = explode(' LIMIT ', $this->postgres);
        
        $this->limit_group = array_pop($limit);
        
        //get order by
        
        $order = explode(' ORDER BY ', implode(' ', $limit));
        
        $this->orderby_group = array_pop($order);
        
        //... TO DO
        
    }
    private function getQueryType() {
        
        
            //ignore UPDATE queries
            if (preg_match('/^UPDATE/', $this->postgres)) 
                    return 'UPDATE';
            
            //ignore SET SESSION queries
            if (preg_match('/^SET SESSION/', $this->postgres)) 
                    return 'SET_SESSION';
            
            //ignore SET SESSION queries
            if (preg_match('/^INSERT INTO/', $this->postgres)) 
                    return 'INSERT';
            
        
    }
    /**
     * displays query before and after changes
     * same query will be launched each time
     */
    private function rw_setExplicitComparing() {
        
        $regex = '/([^\s!<>]+)\s*=\s*(\d+)\s*/';
        
        if (preg_match($regex, $this->postgres, $m)){
            
            //if comparing integer 
            if (preg_match('/^\d*$/', trim($m[1])))
                    return;
                
            $this->showChanges(0);
            
            $this->postgres =  preg_replace($regex, ' CAST($1 AS NUMERIC) = $2 ', $this->postgres);

            $this->showChanges(1);
            
        }
    }
    /**
     * Rewrite some specific cases,
     * also for testing
     * 
     * 
     */
    private function rw_SpecialCases() {
        
        
        if (strpos($this->postgres, 't.*, tt.*' ) !== FALSE
                && strpos($this->postgres, 'wp_termmeta.meta_value+0' ) !== FALSE){
            
            $this->showChanges(0);
            
            $this->postgres = str_replace('t.*, tt.*', 't.*, tt.*,wp_termmeta.meta_value', $this->postgres);
            $this->postgres = str_replace('wp_termmeta.meta_value+0', 'wp_termmeta.meta_value', $this->postgres);
            
            $this->showChanges(1);
            
        }
        
        
    }
    
    private function rw_SpecialCases_1() {
        
        
        if (strpos($this->postgres, 'SELECT  posts.id as refund_id' ) != FALSE){
            
            $this->showChanges(0);
            
            $this->postgres = str_replace('SELECT  posts.id as refund_id', 'SELECT  posts."ID" as refund_id', $this->postgres);
            
            $this->showChanges(1);
            
        }
        
    }
    
    private function rw_SpecialCases_2() {
        
        if (strpos($this->postgres, 'comment_post_ID') !== FALSE){
            
            $this->showChanges(0);
            
            $this->postgres = str_replace('comment_post_ID', 'comment_post_id', $this->postgres);
            
            $this->showChanges(1);
            
        }
        
    }
    
    private function rw_SpecialCases_3() {
        
        
        if (preg_match('/wp_posts\s+AS\s+posts/', $this->postgres)
                && strpos($this->postgres, 'posts.id')){
            
            
            $this->showChanges(0);
            
            $this->postgres = str_replace('posts.id', 'posts."ID"', $this->postgres);
            
            $this->showChanges(1);
            
        }
        
    }
    
    
    /**
     * Rewrite DATE_FORMAT to other syntax
     * 
     * Example:
     * mysql - DATE_FORMAT( wp_mylisting_visits.time, '%Y-%m-%d %H:00:00' ) as date
     * postgres - TO_CHAR( wp_mylisting_visits.time ::  DATE, 'YYY-mm-dd HH24:00:00' ) as date
     * 
     */
    private function rw_DATE_FORMAT() {
        
        $regex = '/DATE_FORMAT\((.*?),(.*?)\)/';

        if (preg_match($regex, $this->postgres)){
            
            $search_terms = [
                '/%Y/',
                '/%m/',
                '/%d/',
                '/%H/',
                $regex
            ];

            $replacements = [
                'YYYY',
                'mm',
                'dd',
                'HH24',
                'TO_CHAR($1 ::  DATE, $2)'
            ];
            
            $this->showChanges(0);
            
            $this->postgres =  preg_replace($search_terms, $replacements, $this->postgres);
            
            $this->showChanges(1);
            
            
        }
         
    }
    /**
     * Rewrite AVG to other syntax
     * 
     * Example:
     * mysql - AVG(meta_value) AS avg_rating
     * postgres - AVG(CAST(meta_value AS DECIMAL(12,2))) AS avg_rating
     * 
     */
    private function rw_AVG() {
        $regex = '/AVG\((.*?)\)/';

        if (preg_match($regex, $this->postgres)){
           
            $search_terms = [
                $regex
            ];

            $replacements = [
                'AVG(CAST($1 AS DECIMAL(12,2)))'
            ];

            $this->showChanges(0);
            
            $this->postgres =  preg_replace($search_terms, $replacements, $this->postgres);
                        
            $this->showChanges(1);
            
        }
        
    }
    /**
     * Rewrite data converstion in MYSQL to POSTGRES syntax
     * 
     * Example:
     * mysql - wp_termmeta.meta_value+0
     * postgres - CAST(wp_termmeta.meta_value AS DECIMAL(12,2))
     * 
     */
    private function rw_DataConversion() {
        $regex = '/([^\s]*)\+0\s/';

        if (preg_match($regex, $this->postgres)){
           
            $search_terms = [
                $regex
            ];

            $replacements = [
                ' $1::INTEGER '
            ];

            $this->showChanges(0);
            
            $this->postgres =  preg_replace($search_terms, $replacements, $this->postgres);
                        
            $this->showChanges(1);
            
        }
        
    }
    /**
     * Rewrite table columns name to lowercase
     * 
     * Example:
     * mysql - wp_commnets.comment_post_ID
     * postgres - wp_commnets.comment_post_id
     * 
     */
    private function rw_ChangeColumnNameCase() {
        
        $search_terms = [
            'comment_ID',
            'comment_post_ID'
        ];

        $replacements = [
            'comment_id',
            'comment_post_id'
        ];

        $this->showChanges(0);
            
        $this->postgres =  str_replace($search_terms, $replacements, $this->postgres);
        
        $this->showChanges(1);
            
    }
    /**
     * Rewrite SUM to eplicit type
     * 
     * Example:
     * mysql - SUM ( order_item_meta__qty.meta_value)
     * postgres - SUM ( order_item_meta__qty.meta_value :: INTEGER)
     * 
     */    
    private function rw_SUM() {
        
        $regex = '/SUM\((.*?)\)/';

        if (preg_match($regex, $this->postgres)){
           
            
            
            $search_terms = [
                $regex
            ];

            $replacements = [
                'SUM($1 :: INTEGER)'
            ];
            
            $this->showChanges(0);
            
            $this->postgres =  preg_replace($search_terms, $replacements, $this->postgres);
            
            $this->showChanges(1);
            
            
        }
         
    }
    /**
     * Rewrite YEAR function to new syntax
     * 
     * Example:
     * mysql - YEAR(wp_posts.post_date)
     * postgres - EXTRACT(YEAR FROM wp_posts.post_date :: DATE)
     * 
     */    
    private function rw_YEAR() {
        
        $regex = '/YEAR\(([^\)]*?)\)/';

        if (preg_match($regex, $this->postgres)){
                       
            $search_terms = [
                $regex
            ];

            $replacements = [
                'EXTRACT(YEAR FROM $1::DATE)'
            ];
            
            $this->showChanges(0);
            
            $this->postgres =  preg_replace($search_terms, $replacements, $this->postgres);
            
            $this->showChanges(1);
            
            
        }
         
    }
    /**
     * Rewrite EXTRACT function to new syntax
     * 
     * Example:
     * mysql - EXTRACT(YEAR FROM value)
     * postgres - EXTRACT(YEAR FROM value::DATE)
     */
    private function rw_EXTRACT() {
        
        $regex = '/EXTRACT\(([^\:)]*?)\)/';

        if (preg_match($regex, $this->postgres)){
                       
            $search_terms = [
                $regex
            ];

            $replacements = [
                'EXTRACT($1::DATE)'
            ];
            
            $this->showChanges(0);
            
            $this->postgres =  preg_replace($search_terms, $replacements, $this->postgres);
            
            $this->showChanges(1);
            
            
        }
         
    }
    
    /**
     * Rewrite UNSIGNED type to new syntax
     * 
     * this method will remove UNSIGNED type conversion from ORDER BY clause
     * 
     * st the moment we reqrite single request without regex, if we get error in other cases we will make few step check.
     * 
     * Example:
     * mysql - ORDER BY  CAST( COALESCE( priority_meta.meta_value, 0 ) AS UNSIGNED )
     * postgres - ORDER BY  COALESCE( priority_meta.meta_value, 0 )
     */
    private function rw_UNSIGNED_IN_OBY() {
        
        $string = 'ORDER BY  CAST( COALESCE( priority_meta.meta_value, 0 ) AS UNSIGNED )';

        if (strpos($this->postgres,$string)){
                       
            $search_terms = [
                $string
            ];

            $replacements = [
                'ORDER BY  priority_meta.meta_value::INTEGER'
            ];
            
            $this->showChanges(0);
            
            $this->postgres =  str_replace($search_terms, $replacements, $this->postgres);
            
            $this->showChanges(1);
            
            
        }
         
    }
    
    /**
     * Solve ERROR:  for SELECT DISTINCT, ORDER BY expressions must appear in select list
     * 
     * when adding DISTINCT in sql we should have all order 
     * by values in select, postgress limitation (may be)
     * 
     * Example:
     * mysql - SELECT field1 FROM table1 WHERE 1 ORDER BY field2 DESC , field3 DESC
     * postgres - SELECT field1,field2,field3 FROM table1 WHERE 1 ORDER BY field2 DESC , field3 DESC
     */
    private function rw_Solve_select_list_error() {
        
        if ( ! strpos($this->postgres, ' DISTINCT ') 
                || ! strpos($this->postgres, ' ORDER BY ') )
                return;
        
        $regex_sel = '/SELECT(.*?) FROM/';
        
        if (preg_match($regex_sel, $this->postgres,$m)){
            
            $select_list = preg_split('/[ ,]/', trim($m[1]));
            
        } else {
            return;
        }
        
        
        if (preg_match('/ORDER BY(.*?) LIMIT /', $this->postgres,$m)){
            
            $regex_group_by = '/ORDER BY(.*?) LIMIT /';
            
            $with_limit = true;
            
            $group_by_list = preg_split('/[ ,]/', trim($m[1]));
            
        } else if (preg_match('/ORDER BY(.*?)$/', $this->postgres,$m)){
            
            $regex_group_by = '/ORDER BY(.*?)$/';
            
            $with_limit = false;
            
            $group_by_list = preg_split('/[ ,]/', trim($m[1]));
            
        } else {
            
            return;
            
        }
        
        $select_list[] = 'DESC';
        
        $select_list[] = 'ASC';
        
        $append_to_select = '';
        
        foreach ($group_by_list as  $group) {
                        
            if (in_array($group, $select_list)
                    || empty($group))
                    
                    continue;
            
            $append_to_select .= ',' . $group;
            
        }
        
        $append_to_group_by = '';
        
        foreach ($select_list as  $select) {
                        
            if (in_array($select, $group_by_list)
                    || empty($select))
                    
                    continue;
            
            $append_to_group_by .= ',' . $select;
            
        }
        
        $this->showChanges(0);

        $this->postgres = preg_replace($regex_sel, "SELECT $1 $append_to_select FROM", $this->postgres);
        
        
//        if ($with_limit)
//            $this->postgres = preg_replace($regex_group_by, "ORDER BY $1 $append_to_group_by LIMIT ", $this->postgres);
//        else
//            $this->postgres = preg_replace($regex_group_by, "ORDER BY $1 $append_to_group_by ", $this->postgres);
        
        $this->showChanges(1);

        
    }
    
    /**
     * Add "posts.post_date" in GROUB BY clause
     * 
     * resolve single error
     * 
     * Example:
     * mysql - GROUP BY vars
     * postgres - GROUP BY vars , posts.post_date
     */
    private function rw_Solve_GROUP_BY_error() {
        
        if (!strpos($this->postgres, "posts.post_date")) return;
        
        if(preg_match('/(GROUP BY.*?)\s*(?: LIMIT | ORDER BY |$)/', $this->postgres, $m)){

            $this->showChanges(0);
            
            str_replace($m[1], '$m,posts.post_date ', $this->postgres);
                    
            $this->showChanges(1);

        }
        
    }
    
    /**
     * Rewrite radians in explore page query type add explicit type definition
     * 
     * 
     * Example:
     * mysql - cos( radians( latitude.meta_value ) )
     * postgres - cos( radians( latitude.meta_value::NUMBER ) )
     */
    private function rw_Radians_to_explicit_type() {
        
        $regex = '/(radians\([^\(\)\']*)\)/';
        
        $temp = $this->postgres;
        
        if (preg_match_all($regex,$temp,$m)){
            
            $m = array_unique($m[1]);
            
            foreach ($m as $string) {
                
                $temp = str_replace($string, "$string::NUMERIC " , $temp);
                
            }
            
            
            $this->showChanges(0);
            
            $this->postgres = $temp;

            $this->showChanges(1);
            
        } 
         
    }
    /**
     * Rewrite HAVING clause
     * 
     * at he moment it will rewrite only distanve case
     * 
     * later this method can be extended for all other cases
     * 
     * after this replace, I get better query speed in MYSQL))))
     * 
     * Example:
     * mysql - HAVING distnace < '10'
     * postgres - AND "EXPRESSION" < '10'
     */
    private function rw_HAVING() {
        
        $regex = '/,(.*?)AS\sdistance,/s';
        
        if (preg_match($regex, $this->postgres,$m)){
            
            
            $this->showChanges(0);
            
            $this->postgres = preg_replace('/HAVING distance/', "AND  $m[1]" , $this->postgres);

            $this->showChanges(1);
            
        }
         
    }
    /**
     * Set expicit comparing for case a<>b
     * 
     * Example:
     * mysql - HAVING distnace < '10'
     * postgres - AND "EXPRESSION" < '10'
     */
    private function rw_Comparing() {
        
        //the issue is caused by string replace in driver_pgsql.php on line 337. 
        //Now testing without that line
        return;
        
        $regex = '/,(.*?)AS\sdistance,/s';
        
        if (preg_match($regex, $this->postgres,$m)){
            
            
            $this->showChanges(0);
            
            $this->postgres = preg_replace('/HAVING distance/', "AND  $m[1]" , $this->postgres);

            $this->showChanges(1);
            
        }
         
    }
}
