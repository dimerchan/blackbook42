<?php
require_once __DIR__ . '/RewriteSQL.php';
require_once __DIR__ . '/../../art/classes.php';

use PHPUnit\Framework\TestCase;

class RewriteSQLTest extends TestCase{
    
    protected $RewriteSQL;
    protected $mysqlDB;
    protected $postgresDB;
    
    static $stepperFile;

    public static function setUpBeforeClass(): void {
        
        //create temporary steps file
        self::$stepperFile = __DIR__ . '/logs/testRewritedFile.log';
        
        if (file_exists(self::$stepperFile)){
            
            //truncate file
            file_put_contents(self::$stepperFile, '');
            
        } else if (FALSE === file_put_contents(self::$stepperFile, '')) {
            
            //steps file can't be created
            throw new Error("Can't create stepper file 'testRewritedFile.txt' on path " . __DIR__);
            
        }
        
        chmod(self::$stepperFile, 777);
        
    }

    public static function tearDownAfterClass(): void
    {
        
        if (file_exists(self::$stepperFile)){
            
            //truncate file
            unlink(self::$stepperFile);
            
            
        }
    }
    
    /**
     * @dataProvider dataProvider
     */
    public function testRw_SpecialCases($sql,$incoming,$rewrited) {
        
        $reflection = new ReflectionClass('RewriteSQL');
        
        $method     = $reflection->getMethod('rw_SpecialCases');
        
        $method->setAccessible(true);
        
        $rewrite    = new RewriteSQL($incoming, $sql);
        
        $before_rw  = $rewrite->postgres;
        
        $method->invoke($rewrite);
        
        $after_rw   =  $rewrite->postgres;
        
        //for next test
        file_put_contents(self::$stepperFile, "\n----S----\n$after_rw\n----E----\n",FILE_APPEND);
        
        
        if (strpos($before_rw, 't.*, tt.*' ) === FALSE 
                
                && strpos($before_rw, 'wp_termmeta.meta_value+0' ) === FALSE){
         
            $this->assertEquals(
                    $before_rw, 
                    
                    $after_rw
                );  
            
        } else if (strpos($before_rw, 't.*, tt.*' ) !== FALSE 
                
                && strpos($before_rw, 'wp_termmeta.meta_value+0' ) !== FALSE){
          
            $this->assertNotEquals(
                    
                        $before_rw, 
                    
                        $after_rw
                    
                    );
            
        } else {
            
            $this->assertTrue(true);
            
        }
        
    }
   
    
    /**
     * @depends testRw_SpecialCases
     * @dataProvider dataProviderFromStepLog
     */
    public function testRw_SpecialCases_1($incoming,$test) {
        
        $reflection = new ReflectionClass('RewriteSQL');
        
        $method     = $reflection->getMethod('rw_SpecialCases_1');
        
        $method->setAccessible(true);
        
        $rewrite    = new RewriteSQL($incoming, '');
        
        $before_rw  = $rewrite->postgres;
        
        $method->invoke($rewrite);
        
        $after_rw   =  $rewrite->postgres;
        
        
        if (strpos($before_rw, 'SELECT  posts.id as refund_id' ) != FALSE){
            
            $this->assertNotEqual($before_rw,$after_rw);
            
        } else {
            
            $this->assertEqual($before_rw,$after_rw);
            
        }
        
        
    }
    
    /**
     * 
     * parsing RewriteSQL.log and return array of sql string
     * 
     * 
     * @return type array
     */
    public function dataProvider(){
        
        $file = file_get_contents(__DIR__ . '/logs/RewriteSQL.log');
        
        $pattern_sql = '/SQL:\n(.*?)\n-----------------SQL END/s';
        $pattern_inc = '/INCOMMING:\n(.*?)\n-----------------INCOMMING END/s';
        $pattern_rew = '/Rewrited:\n(.*?)\n-----------------REWRITED END/s';
        
        preg_match_all($pattern_sql, $file, $matches_sql);
        preg_match_all($pattern_inc, $file, $matches_inc);
        preg_match_all($pattern_rew, $file, $matches_rew);
        
        if (count($matches_inc) != count($matches_inc) 
                || count($matches_inc) != count($matches_rew) ){
            
            exit('LOG FILE STRUCTURE CoRRuPTED');
        }
        
        $sql_array = $matches_sql[1];
        
        $incoming_array = $matches_inc[1];
        
        $rewrited_array = $matches_rew[1];
        
        $results = [];
        
        foreach ($sql_array as $key => $value) {
            
            $results[] = [
                $value,
                $incoming_array[$key],
                $rewrited_array[$key]
            ];
            
        }
        
        unset($file);
        
        unset($matches_sql);
        unset($matches_inc);
        unset($matches_rew);
        
        unset($sql_array);
        unset($incoming_array);
        unset($rewrited_array);
        
        return $results;
        
    }
    
    /**
     * extract results of previuse test
     * 
     * @return type array
     */
    public function dataProviderFromStepLog() {
        
        $file = file_get_contents(self::$stepperFile);
        
        if (empty($file)){
            throw new Error('Stepper File Empty');
        }
        
        $pattern_step = '/S----\n(.*?)----E\n/s';
        
        preg_match_all($pattern_step, $file, $matches_step);
        
        $step_array = $matches_step[1];
        
        $results = [];
        
        foreach ($step_array as $key => $value) {
            
            $results[] = [
                $value
            ];
            
        }
        
        unset($file);
        
        unset($matches_step);
        
        unset($step_array);
        
        //truncate file
        file_put_contents(self::$stepperFile, '');

        return $results;
        
    }
}