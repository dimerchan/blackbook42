<?php

// Enqueue child theme style.css
add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style( 'child-style', get_stylesheet_uri() );

    if ( is_rtl() ) {
    	wp_enqueue_style( 'mylisting-rtl', get_template_directory_uri() . '/rtl.css', [], wp_get_theme()->get('Version') );
    }
}, 500 );

// Happy Coding :)

// Include this on your functions.php
function log_sql_queries($text_query){
    //Uncomment me if you want a lot of info about where the sql query comes from and what action started it off
    $traces = debug_backtrace();
    $i = 0;
    foreach ($traces as $tobj => $trace) {
        if($trace['function'] == 'do_action'){
            $args = $trace['args'];
        }
        error_log("TRACE:$i:"  . $trace['function'] . print_r($args,1));
        $i++;
    }
    error_log("INFO:SQL: " . $text_query);
    return $text_query;
}
//add_filter( 'posts_request', 'log_sql_queries', 500 );



function debug_to_console_e() {
    $output =  jp_magic_search_filter_fun('david');
    if (is_array($output))
        $output = implode(',', $output);



    echo "<script>console.log('Debug Objects: " . $output . "' );</script>";


}



