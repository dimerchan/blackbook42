MyListing.Helpers.css_escape = function (t) {
    if (0 == arguments.length) throw new TypeError("`CSS.escape` requires an argument.");
    for (var i, e = String(t), a = e.length, n = -1, s = "", o = e.charCodeAt(0); ++n < a;) 0 != (i = e.charCodeAt(n)) ? s += i >= 1 && i <= 31 || 127 == i || 0 == n && i >= 48 && i <= 57 || 1 == n && i >= 48 && i <= 57 && 45 == o ? "\\" + i.toString(16) + " " : (0 != n || 1 != a || 45 != i) && (i >= 128 || 45 == i || 95 == i || i >= 48 && i <= 57 || i >= 65 && i <= 90 || i >= 97 && i <= 122) ? e.charAt(n) : "\\" + e.charAt(n) : s += "�";
    return s
}, jQuery(function (t) {
    var i = {postid: t("#case27-post-id").val(), authid: t("#case27-author-id").val()};

    function e(e) {
        var a;
        (a = e).contents.hide(), a.pagination.hide(), a.loader.show(), t.ajax({
            url: CASE27.ajax_url + "?action=get_related_listings_by_id&security=" + CASE27.ajax_nonce,
            type: "POST",
            dataType: "json",
            data: {listing_id: i.postid, page: e.page, listing_type: e.type},
            success: function (t) {
                var i;
                e.contents.html(t.html), e.pagination.html(t.pagination), e.menu_spinner.hide(), e.counter.html(t.formatted_count).removeClass("hide"), (i = e).loader.hide(), i.contents.fadeIn(150), i.pagination.fadeIn(150), setTimeout(function () {
                    jQuery(".lf-background-carousel").owlCarousel({
                        margin: 20,
                        items: 1,
                        loop: !0
                    }), jQuery('[data-toggle="tooltip"]').tooltip({trigger: "hover"})
                }, 10)
            }
        })
    }

    t(".toggle-tab-type-related_listings").each(function (i, a) {
        var n = {};
        n.menu = t(this), n.id = n.menu.data("section-id"), n.section = t("#listing_tab_" + n.id), n.type = n.menu.data("options").type, n.loader = n.section.find(".tab-loader"), n.contents = n.section.find(".tab-contents"), n.pagination = n.section.find(".tab-pagination"), n.counter = n.menu.find(".items-counter"), n.menu_spinner = n.menu.find(".tab-spinner"), n.page = 0, e(n), n.pagination.on("click", "a", function (i) {
            i.preventDefault(), n.page = parseInt(t(this).data("page"), 10) - 1, e(n)
        })
    })
}), function (t) {
    var i = t(".listing-tab-toggle").first().data("section-id");
    t(".listing-tab-toggle").on("click", function (e) {
        e.preventDefault(), t(".profile-menu li.active").removeClass("active"), t(this).parent().addClass("active");
        var a = t(".listing-tab.tab-active"), n = t(this).data("section-id"),
            s = t(".listing-tab#listing_tab_" + MyListing.Helpers.css_escape(n));
        if (a.attr("id") === "listing_tab_" + n) return a.addClass("tab-same"), void setTimeout(function () {
            a.removeClass("tab-same")
        }, 100);
        a.addClass("tab-hiding"), setTimeout(function () {
            a.removeClass("tab-active tab-hiding").addClass("tab-hidden"), s.addClass("tab-showing"), setTimeout(function () {
                s.removeClass("tab-hidden tab-showing").addClass("tab-active").trigger("mylisting:single:tab-switched")
            }, 25)
        }, 200), history.replaceState(null, null, i === n ? " " : "#" + n)
    })
}(jQuery), function (t) {
    var i = window.location.hash.substr(1),
        e = t(".listing-tab-toggle#listing_tab_" + MyListing.Helpers.css_escape(i) + "_toggle");
    i.length && e.length ? e.first().click() : t(".listing-tab-toggle").first().click()
}(jQuery), function (t) {
    t(".listing-tab.tab-layout-masonry").on("mylisting:single:tab-switched", function () {
        if (!t(this).hasClass("pre-init")) return t(this).find(".listing-tab-grid").isotope("layout");
        t(this).removeClass("pre-init").find(".listing-tab-grid").isotope(t("body").hasClass("rtl") ? {originLeft: !1} : {})
    });
    var i = MyListing.Helpers.debounce(function () {
        t(".listing-tab.tab-layout-masonry:not(.pre-init) .listing-tab-grid").isotope("layout")
    }, 100);
    t(".listing-tab.tab-layout-masonry .grid-item").each(function (t, e) {
        new ResizeSensor(e, i)
    })
}(jQuery), jQuery(function (t) {
    !function () {
        if (t("#report-listing-modal").length) {
            var i = t("#report-listing-modal"), e = i.find(".validation-message"), a = t("#case27-post-id").val();
            i.find(".report-submit").on("click", function (n) {
                n.preventDefault(), e.hide(), i.find(".sign-in-box").addClass("cts-processing-login"), t.ajax({
                    url: CASE27.ajax_url + "?action=report_listing&security=" + CASE27.ajax_nonce,
                    type: "POST",
                    dataType: "json",
                    data: {listing_id: a, content: i.find(".report-content").val()},
                    success: function (t) {
                        i.find(".sign-in-box").removeClass("cts-processing-login"), "error" === t.status && e.html("<em>" + t.message + "</em>").show(), "success" === t.status && i.find(".report-wrapper").html('<div class="submit-message"><em>' + t.message + "</em></div>")
                    }
                })
            })
        }
    }(), t(".comments-list .comment .review-galleries .gallery-item img").on("click", function (i) {
        i.preventDefault();
        var e = [], a = this, n = 0;
        t(this).parents(".review-galleries").find(".gallery-item img").each(function (t, i) {
            if (!i.dataset.fullSizeSrc || !i.dataset.fullSizeWidth || !i.dataset.fullSizeHeight) return !1;
            i.dataset.fullWidth = i.dataset.fullSizeWidth, i.dataset.fullHeight = i.dataset.fullSizeHeight, e.push({
                src: i.dataset.fullSizeSrc,
                w: i.dataset.fullWidth || 0,
                h: i.dataset.fullHeight || 0,
                el: i
            }), i == a && (n = t)
        }), new MyListing.PhotoSwipe(e, n)
    })
}), jQuery(function (t) {
    var i = {postid: t("#case27-post-id").val(), authid: t("#case27-author-id").val()};

    function e(e) {
        var a;
        (a = e).contents.hide(), a.pagination.hide(), a.loader.show(), t.ajax({
            url: CASE27.ajax_url + "?action=mylisting_get_products&security=" + CASE27.ajax_nonce,
            type: "POST",
            dataType: "json",
            data: {products: e.products, page: e.page, author_id: i.authid},
            success: function (t) {
                var i;
                e.contents.html(t.html), e.pagination.html(t.pagination), e.counter.html(t.formatted_count), (i = e).loader.hide(), i.contents.fadeIn(150), i.pagination.fadeIn(150)
            }
        })
    }

    t(".toggle-tab-type-store").each(function (i, a) {
        var n = {};
        n.menu = t(this), n.id = n.menu.data("section-id"), n.section = t("#listing_tab_" + n.id), n.loader = n.section.find(".store-loader"), n.contents = n.section.find(".store-contents"), n.pagination = n.section.find(".store-pagination"), n.counter = n.menu.find(".items-counter"), n.products = n.menu.data("options").products, n.page = 0, e(n), n.pagination.on("click", "a", function (i) {
            i.preventDefault(), n.page = parseInt(t(this).data("page"), 10) - 1, e(n)
        })
    })
});
//# sourceMappingURL=single-listing.js.map
