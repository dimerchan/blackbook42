<?php ob_start() ?>
    <li class="item-preview" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php esc_attr_e( 'Quick view', 'my-listing' ) ?>">
        <a href="#" type="button" class="c27-toggle-quick-view-modal" data-id="<?php echo esc_attr( $listing->get_id() ); ?>"><i class="material-icons">zoom_in</i></a>
    </li>
<?php $quick_view_button = ob_get_clean() ?>

<?php ob_start() ?>
    <li data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php esc_attr_e( 'Bookmark', 'my-listing' ) ?>">
        <a class="c27-bookmark-button <?php echo mylisting()->bookmarks()->is_bookmarked($listing->get_id(), get_current_user_id()) ? 'bookmarked' : '' ?>"
           data-listing-id="<?php echo esc_attr( $listing->get_id() ) ?>" data-nonce="<?php echo esc_attr( wp_create_nonce('c27_bookmark_nonce') ) ?>">
            <i class="material-icons">favorite_border</i>
        </a>
    </li>
<?php $bookmark_button = ob_get_clean() ?>




<a href="<?php echo esc_url( $listing->get_link() ) ?>" class="card-link"><div class="card">
    <?php if ($options['background']['type'] == 'gallery' && ( $gallery = $listing->get_field( 'gallery' ) ) ): ?>
        <?php foreach (array_slice($gallery, 0, 1) as $gallery_image): ?>
            <img src="<?php echo esc_url( job_manager_get_resized_image( $gallery_image, 'large' ) ) ?>" alt="" class="img-fluid">
        <?php endforeach ?>
    <?php else: $options['background']['type'] = 'image'; endif; // Fallback to cover image if no gallery images are present ?>

    <?php if ($options['background']['type'] == 'image' && ( $cover = $listing->get_cover_image( 'large' ) ) ): ?>
        <img src="<?php echo esc_url( $cover ) ?>" alt="" class="img-fluid">
    <?php endif ?>
        <span class="card-grad"></span>
        <div class="card-text">
            <h3><?php echo $listing->get_name() ?></h3>
            <button class="card-actions quick-view"></button>
            <button class="card-actions add-fav"></button>
        </div>
</div></a>

    <?php foreach ((array) $options['footer']['sections'] as $section): ?>
        <!-- CATEGORIES SECTION -->
        <?php if ($section['type'] == 'categories'):
            // Keys = taxonomy name
            // Value = taxonomy field name
            $taxonomies = array_merge( [
                'job_listing_category' => 'job_category',
                'case27_job_listing_tags' => 'job_tags',
                'region' => 'region',
            ], mylisting_custom_taxonomies( 'slug', 'slug' ) );

            $taxonomy = ! empty( $section['taxonomy'] ) ? $section['taxonomy'] : 'job_listing_category';

            if ( ! isset( $taxonomies[ $taxonomy ] ) ) {
                continue;
            }

            if ( ! ( $terms = $listing->get_field( $taxonomies[ $taxonomy ] ) ) ) {
                continue;
            }

            $footer_section_count++;
            ?>

                <ul class="c27-listing-preview-category-list" style="margin-bottom: 40px!important;">

                    <?php if ( $terms ):
                        $category_count = count( $terms );
                        $first_category = array_shift( $terms );
                        $first_ctg = new MyListing\Src\Term( $first_category );
                        $category_names = array_map(function($category) {
                            return $category->name;
                        }, $terms);
                        $categories_string = join('<br>', $category_names);
                        ?>
                        <li>
                            <a href="<?php echo esc_url( $first_ctg->get_link() ) ?>">
                                <span class="cat-icon" style="">
                                    <?php echo $first_ctg->get_icon([ 'background' => false ]) ?>
                                </span>
                                <span class="category-name"><?php echo esc_html( $first_ctg->get_name() ) ?></span>
                            </a>
                        </li>

                        <?php if (count($terms)): ?>
                        <li data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo esc_attr( $categories_string ) ?>" data-html="true">
                            <div class="categories-dropdown dropdown c27-more-categories">
                                <a href="#other-categories">
                                    <span class="cat-icon cat-more">+<?php echo $category_count - 1 ?></span>
                                </a>
                            </div>
                        </li>
                    <?php endif ?>
                    <?php endif ?>
                </ul>

                <div class="ld-info">
                    <ul>
                        <?php if (isset($section['show_quick_view_button']) && $section['show_quick_view_button'] == 'yes'): ?>
                            <?php echo $quick_view_button ?>
                        <?php endif ?>
                        <?php if (isset($section['show_bookmark_button']) && $section['show_bookmark_button'] == 'yes'): ?>
                            <?php echo $bookmark_button ?>
                        <?php endif ?>
                    </ul>
                </div>
        <?php endif ?>
    <?php endforeach; ?>







<!--
<div class="lf-item <?php echo esc_attr( 'lf-item-'.$options['template'] ) ?>" data-template="alternate">
    <a href="<?php echo esc_url( $listing->get_link() ) ?>">

        <?php
        /**
         * Include section overlay template.
         *
         * @since 1.0
         */
        require locate_template( 'templates/single-listing/previews/partials/overlay.php' ) ?>

        <!-- BACKGROUND GALLERY -->
        <?php if ($options['background']['type'] == 'gallery' && ( $gallery = $listing->get_field( 'gallery' ) ) ): ?>
            <div class="owl-carousel lf-background-carousel">
                <?php foreach (array_slice($gallery, 0, 3) as $gallery_image): ?>
                    <div class="item">
                        <div class="lf-background" style="background-image: url('<?php echo esc_url( job_manager_get_resized_image( $gallery_image, 'large' ) ) ?>');"></div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php else: $options['background']['type'] = 'image'; endif; // Fallback to cover image if no gallery images are present ?>

        <!-- BACKGROUND IMAGE -->
        <?php if ($options['background']['type'] == 'image' && ( $cover = $listing->get_cover_image( 'large' ) ) ): ?>
            <div class="lf-background" style="background-image: url('<?php echo esc_url( $cover ) ?>');"></div>
        <?php endif ?>

        <div class="lf-item-info-2">
            <?php if ( $logo = $listing->get_logo() ): ?>
                <div class="lf-avatar" style="background-image: url('<?php echo esc_url( $logo ) ?>')"></div>
            <?php endif ?>

            <h4 class="case27-secondary-text listing-preview-title">
                <?php echo $listing->get_name() ?>
                <?php if ( $listing->is_verified() ): ?>
                    <span class="verified-badge"><i class="fa fa-check"></i></span>
                <?php endif ?>
            </h4>

            <?php if ( $tagline ): ?>
                <h6><?php echo esc_html( $tagline ) ?></h6>
            <?php endif ?>

            <?php if ( ! empty( $info_fields ) ): ?>
                <ul class="lf-contact">
                    <?php foreach ( $info_fields as $info_field ): ?>
                         <li>
                            <i class="<?php echo esc_attr( $info_field['icon'] ) ?> sm-icon"></i>
                            <?php echo esc_html( $info_field['content'] ) ?>
                        </li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
        </div>

        <?php
        /**
         * Include head buttons template.
         *
         * @since 1.0
         */
        require locate_template( 'templates/single-listing/previews/partials/head-buttons.php' ) ?>
    </a>

    <?php
    /**
     * Include gallery background nav.
     *
     * @since 1.0
     */
    if ( $options['background']['type'] === 'gallery' ) {
        require locate_template( 'templates/single-listing/previews/partials/gallery-nav.php' );
    } ?>
</div>

<?php
/**
 * Include footer sections template.
 *
 * @since 1.0
 */
require locate_template( 'templates/single-listing/previews/partials/footer-sections.php' ) ?>
-->
