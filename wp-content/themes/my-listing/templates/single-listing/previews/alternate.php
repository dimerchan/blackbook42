<?php ob_start() ?>
    <li class="item-preview" data-toggle="tooltip" data-placement="top"
        data-original-title="<?php esc_attr_e('Quick view', 'my-listing') ?>">
        <a href="#" type="button" class="c27-toggle-quick-view-modal"
           data-id="<?php echo esc_attr($listing->get_id()); ?>">
            <button class="card-actions quick-view"></button>
        </a>
    </li>
<?php $quick_view_button = ob_get_clean() ?>

<?php ob_start() ?>
    <li data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php esc_attr_e( 'Bookmark', 'my-listing' ) ?>">
        <a class="c27-bookmark-button <?php echo mylisting()->bookmarks()->is_bookmarked($listing->get_id(), get_current_user_id()) ? 'bookmarked' : '' ?>"
           data-listing-id="<?php echo esc_attr( $listing->get_id() ) ?>" data-nonce="<?php echo esc_attr( wp_create_nonce('c27_bookmark_nonce') ) ?>">
            <button class="card-actions add-fav"></button>
        </a>
    </li>
<?php $bookmark_button = ob_get_clean() ?>
    <!--                                    <button class="card-actions quick-view"></button>-->
    <!--                                    <button class="card-actions add-fav"></button>-->
<?php
$pictureUrl = '';
if ($options['background']['type'] == 'gallery' && ( $gallery = $listing->get_field( 'gallery' ) ) )
    foreach (array_slice($gallery, 0, 1) as $gallery_image)
        $pictureUrl = esc_url( job_manager_get_resized_image( $gallery_image, 'large' ) );
else
    $options['background']['type'] = 'image'; // Fallback to cover image if no gallery images are present

if ($options['background']['type'] == 'image' && ( $cover = $listing->get_cover_image( 'large' ) ) )
    $pictureUrl = esc_url( $cover );

$listing_card_css_id = uniqid("ca");
?>

<a href="<?php echo esc_url( $listing->get_link() ) ?>" class="card-link"><div class="card" style="overflow:hidden!important;">
    <div id="<?php echo $listing_card_css_id ?>" class="img-fluid" style="height:380px!important; width: auto!important; background-position: top center;  background-size: cover; background-repeat: no-repeat; background-image:url('<?php echo $pictureUrl; ?>')"></div>
        <span class="card-grad"></span>
        <div class="card-text">
            <h3><?php echo $listing->get_name() ?></h3>

            <!-- FOOTER SECTIONS -->
            <?php $footer_section_count = 0; ?>
            <?php if ($options['footer']['sections']): ?>
                <?php foreach ((array) $options['footer']['sections'] as $section): ?>
                    <?php if ($section['type'] == 'actions' || $section['type'] == 'details'): ?>
                        <?php if (
                            ( isset($section['show_quick_view_button']) && $section['show_quick_view_button'] == 'yes' ) ||
                            ( isset($section['show_bookmark_button']) && $section['show_bookmark_button'] == 'yes' )
                        ): $footer_section_count++; ?>


                            <ul>
                                <?php if (isset($section['show_quick_view_button']) && $section['show_quick_view_button'] == 'yes'): ?>
                                    <?php echo $quick_view_button ?>
                                <?php endif ?>
                                <?php if (isset($section['show_bookmark_button']) && $section['show_bookmark_button'] == 'yes'): ?>
                                    <?php echo $bookmark_button ?>
                                <?php endif ?>
                            </ul>

                        <?php endif ?>
                    <?php endif ?>
                <?php endforeach ?>
            <?php endif ?>
        </div>
</div></a>

    <?php foreach ((array) $options['footer']['sections'] as $section): ?>
        <!-- CATEGORIES SECTION -->
        <?php if ($section['type'] == 'categories'):
            // Keys = taxonomy name
            // Value = taxonomy field name
            $taxonomies = array_merge( [
                'job_listing_category' => 'job_category',
                'case27_job_listing_tags' => 'job_tags',
                'region' => 'region',
            ], mylisting_custom_taxonomies( 'slug', 'slug' ) );

            $taxonomy = ! empty( $section['taxonomy'] ) ? $section['taxonomy'] : 'job_listing_category';

            if ( ! isset( $taxonomies[ $taxonomy ] ) ) {
                continue;
            }

            if ( ! ( $terms = $listing->get_field( $taxonomies[ $taxonomy ] ) ) ) {
                continue;
            }

            $footer_section_count++;
            ?>

                <ul class="c27-listing-preview-category-list">

                    <?php if ( $terms ):
                        $category_count = count( $terms );
                        $first_category = array_shift( $terms );
                        $first_ctg = new MyListing\Src\Term( $first_category );
                        $placeholder_image = $first_ctg->get_icon([], true);
                        $category_names = array_map(function($category) {
                            return $category->name;
                        }, $terms);
                        $categories_string = join('<br>', $category_names);
                        // set css for placeholder image
                        if($placeholder_image != null && $pictureUrl === ''):
                        ?>
                            <style>
                                #<?php echo $listing_card_css_id?> {
                                    background-image:url('<?php echo $placeholder_image; ?>')!important;
                                    background-size: 25%!important;
                                    background-position-y: center!important;
                                    background-color: #3E7ADC;
                                }
                            </style>
                        <?php endif; ?>
                        <li>
                            <a href="<?php echo esc_url( $first_ctg->get_link() ) ?>">
                                <span class="cat-icon" style="">
                                    <?php echo $first_ctg->get_icon([ 'background' => false ]) ?>
                                </span>
                                <span class="category-name"><?php echo esc_html( $first_ctg->get_name() ) ?></span>
                            </a>
                        </li>

                        <?php if (count($terms)): ?>
                        <li data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo esc_attr( $categories_string ) ?>" data-html="true">
                            <div class="categories-dropdown dropdown c27-more-categories">
                                <a href="#other-categories">
                                    <span class="cat-icon cat-more">+<?php echo $category_count - 1 ?></span>
                                </a>
                            </div>
                        </li>
                    <?php endif ?>
                    <?php endif ?>
                </ul>


        <?php endif ?>
    <?php endforeach; ?>