<?php ob_start() ?>
    <li class="item-preview" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php esc_attr_e( 'Quick view', 'my-listing' ) ?>">
        <a href="#" type="button" class="c27-toggle-quick-view-modal" data-id="<?php echo esc_attr( $listing->get_id() ); ?>"><i class="material-icons">zoom_in</i></a>
    </li>
<?php $quick_view_button = ob_get_clean() ?>

<?php ob_start() ?>
    <li data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php esc_attr_e( 'Bookmark', 'my-listing' ) ?>">
        <a class="c27-bookmark-button <?php echo mylisting()->bookmarks()->is_bookmarked($listing->get_id(), get_current_user_id()) ? 'bookmarked' : '' ?>"
           data-listing-id="<?php echo esc_attr( $listing->get_id() ) ?>" data-nonce="<?php echo esc_attr( wp_create_nonce('c27_bookmark_nonce') ) ?>">
           <i class="material-icons">favorite_border</i>
        </a>
    </li>
<?php $bookmark_button = ob_get_clean() ?>

<!-- FOOTER SECTIONS -->
<?php $footer_section_count = 0; ?>
<?php if ($options['footer']['sections']): ?>
    <?php foreach ((array) $options['footer']['sections'] as $section): ?>
        <?php if ($section['type'] == 'actions' || $section['type'] == 'details'): ?>
            <?php if (
                ( isset($section['show_quick_view_button']) && $section['show_quick_view_button'] == 'yes' ) ||
                ( isset($section['show_bookmark_button']) && $section['show_bookmark_button'] == 'yes' )
             ): $footer_section_count++; ?>
                <div class="listing-details actions c27-footer-section">
                    <div class="ld-info">
                        <ul>
                            <?php if (isset($section['show_quick_view_button']) && $section['show_quick_view_button'] == 'yes'): ?>
                                <?php echo $quick_view_button ?>
                            <?php endif ?>
                            <?php if (isset($section['show_bookmark_button']) && $section['show_bookmark_button'] == 'yes'): ?>
                                <?php echo $bookmark_button ?>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            <?php endif ?>
        <?php endif ?>
    <?php endforeach ?>
<?php endif ?>

<?php if ( $footer_section_count < 1 ): ?>
    <div class="c27-footer-empty"></div>
<?php endif ?>