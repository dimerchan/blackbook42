<?php
$css_class_mapping =  [
        'celebrities' => 'celeb-btn',
        'place' => 'hang-btn',
        'medical-team' => 'med-btn',
        'service-provider' => 'beauty-btn',
        'product-procedure' => 'prod-btn'
];
global $post;
$post_slug = $post->post_name;
?>
<div class="explore explore-types">
    <section class="explore">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="explore finder-title">
<!--                        <h3>search in other categories</h3>-->
                    </div>
                    <ul class="big-categories">
                        <?php foreach ( $explore->store['listing_types'] as $listing_type ): ?>
                            <li class="type-<?php echo esc_attr( $listing_type->get_slug() ) ?>"
                                :class="state.activeListingType == '<?php echo esc_attr( $listing_type->get_slug() ) ?>'  ? 'active' : ''">
                                <a class="btn btn-ghost <?php echo $css_class_mapping[$listing_type->get_slug()]; ?>" @click.prevent="state.activeListingType = <?php echo c27()->encode_attr( $listing_type->get_slug() ) ?>; state.activeListingTypeData.name = <?php echo c27()->encode_attr( $listing_type->get_plural_name() ) ?>; state.activeListingTypeData.icon = '<?php echo esc_attr( $listing_type->get_setting( 'icon' ) ) ?>'; state.activeTab = 'search-form';">
                                    <!--                                            <i class="--><?php //echo esc_attr( $listing_type->get_setting( 'icon' ) ) ?><!--"></i>-->
                                    <?php echo esc_html( $listing_type->get_plural_name() ) ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <p>&nbsp</p>
                <p>&nbsp</p>
                <p>&nbsp</p>
            </div>
        </div>
    </section>
</div>
<div class="explore-head" id="finderSearch" :class="state.mobileTab === 'filters'? '': 'hide-search'">
	<?php if ( count( $explore->store['listing_types'] ) > 1 ): ?>
        <?php if ($post_slug == 'new-explore'): ?>
            <?php foreach ($explore->store['listing_types'] as $type): ?>

                <?php $GLOBALS['c27-facets-vue-object'][ $type->get_slug() ] = []; ?>

                <div class="container-fluid">
                    <div class="search-area" id="search-form" :class="state.activeListingType == '<?php echo esc_attr( $type->get_slug() ) ?>'  ? '' : 'inactive'">
                        <div class="row">
                            <div v-show="state.activeListingType == '<?php echo esc_attr( $type->get_slug() ) ?>'" class="search-filters type-<?php echo esc_attr( $type->get_slug() ) ?>">
                                <div class="col-sm-12"><h1>Explore <span class="blue"><?php echo $type->get_plural_name(); ?></span></h1></div>
                                <div class="light-forms filter-wrapper">

                                    <?php foreach ((array) $type->get_search_filters() as $facet): ?>

                                        <?php if ( $facet['type'] == 'order' ): ?>
                                            <?php continue; ?>
                                        <?php endif ?>

                                        <?php if ($facet['type'] == 'text'): ?>
                                            <div class="form search-form double-search">
                                        <?php endif ?>
                                        <?php c27()->get_partial( "facets/{$facet['type']}", [
                                        'facet' => $facet,
                                        'listing_type' => $type->get_slug(),
                                        'type' => $type,
                                    ] ) ?>

                                        <?php if ($facet['type'] == 'location'): ?>
                                            </div>
                                        <?php endif ?>

                                    <?php endforeach ?>

                                    <?php $GLOBALS['c27-facets-vue-object'][ $type->get_slug() ]['page'] = ( $pg >= 1 ? $pg - 1 : 0 ); ?>

                                </div>
                                <div class="form-group fc-search">
                                    <a
                                            href="#"
                                            class="buttons button-2 full-width button-animated c27-explore-search-button"
                                            @click.prevent="state.mobileTab = 'results'; mobile.matches ? _getListings() : getListings(); _resultsScrollTop();"
                                    >
                                        <?php _e( 'Search', 'my-listing' ) ?><i class="mi keyboard_arrow_right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>

        <?php else: ?>


            <?php foreach ($explore->store['listing_types'] as $type): ?>

                <?php $GLOBALS['c27-facets-vue-object'][ $type->get_slug() ] = []; ?>


                <div class="container-fluid">
                    <div class="search-area">
                        <div class="row">
                            <div v-show="state.activeListingType == '<?php echo esc_attr( $type->get_slug() ) ?>'" class="search-filters type-<?php echo esc_attr( $type->get_slug() ) ?>">
                                <div class="light-forms filter-wrapper">
                                    <?php foreach ((array) $type->get_search_filters() as $facet): ?>

                                        <?php if ( $facet['type'] == 'order' ): ?>
                                            <?php continue; ?>
                                        <?php endif ?>

                                        <?php if ($facet['type'] == 'text'): ?>
                                            <div class="form search-form double-search">
                                        <?php endif ?>
                                        <?php c27()->get_partial( "facets/{$facet['type']}", [
                                        'facet' => $facet,
                                        'listing_type' => $type->get_slug(),
                                        'type' => $type,
                                    ] ) ?>

                                        <?php if ($facet['type'] == 'location'): ?>
                                            </div>
                                        <?php endif ?>

                                    <?php endforeach ?>

                                    <?php $GLOBALS['c27-facets-vue-object'][ $type->get_slug() ]['page'] = ( $pg >= 1 ? $pg - 1 : 0 ); ?>

                                </div>
<!--                                <div class="form-group fc-search">-->
<!--                                    <a-->
<!--                                            href="#"-->
<!--                                            class="buttons button-2 full-width button-animated c27-explore-search-button"-->
<!--                                            @click.prevent="state.mobileTab = 'results'; mobile.matches ? _getListings() : getListings(); _resultsScrollTop();"-->
<!--                                    >-->
<!--                                        --><?php //_e( 'Search', 'my-listing' ) ?><!--<i class="mi keyboard_arrow_right"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        <?php endif ?>
	<?php endif ?>
</div>

<!--<div class="explore-head">-->
<!--    --><?php //if ( count( $explore->store['listing_types'] ) > 1 ): ?>
<!--        <div class="explore-types">-->
<!--            <div class="finder-title">-->
<!--                <h2 class="case27-primary-text">--><?php //echo esc_html( $data['title'] ) ?><!--</h2>-->
<!--            </div>-->
<!--            --><?php //foreach ( $explore->store['listing_types'] as $listing_type ): ?>
<!--                <div class="type---><?php //echo esc_attr( $listing_type->get_slug() ) ?><!--"-->
<!--                     :class="state.activeListingType == '--><?php //echo esc_attr( $listing_type->get_slug() ) ?><!--'  ? 'active' : ''">-->
<!--                    <a @click.prevent="state.activeListingType = --><?php //echo c27()->encode_attr( $listing_type->get_slug() ) ?><!--; state.activeListingTypeData.name = --><?php //echo c27()->encode_attr( $listing_type->get_plural_name() ) ?><!--; state.activeListingTypeData.icon = '--><?php //echo esc_attr( $listing_type->get_setting( 'icon' ) ) ?><!--'; state.activeTab = 'search-form';">-->
<!--                        <div class="type-info">-->
<!--                            <i class="--><?php //echo esc_attr( $listing_type->get_setting( 'icon' ) ) ?><!--"></i>-->
<!--                            <h4>--><?php //echo esc_html( $listing_type->get_plural_name() ) ?><!--</h4>-->
<!--                        </div>-->
<!--                    </a>-->
<!--                </div>-->
<!--            --><?php //endforeach ?>
<!--        </div>-->
<!--    --><?php //endif ?>
<!--</div>-->