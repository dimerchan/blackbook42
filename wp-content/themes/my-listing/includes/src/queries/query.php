<?php

namespace MyListing\Src\Queries;

use WP_Job_Manager_Cache_Helper;
use WP_Query;

if (!defined('ABSPATH')) {
    exit;
}

class Query
{

    public function __construct()
    {
        add_action(sprintf('wp_ajax_%s', $this->action), [$this, 'handle']);
        add_action(sprintf('wp_ajax_nopriv_%s', $this->action), [$this, 'handle']);
    }

    public function send($args = [])
    {
        $this->output($this->query($args), !empty($args['output']) ? $args['output'] : []);
    }

    public function output($query, $args = [])
    {
        ob_start();

        $result = [];
        $result['data'] = [];
        $result['found_jobs'] = false;
        $form_data = !empty($_REQUEST['form_data']) ? $_REQUEST['form_data'] : [];
        if (CASE27_ENV === 'dev') {
            $result['args'] = $args;
            $result['sql'] = $query->request;
        }

        if (empty($form_data['page']) && isset($_REQUEST['page'])) {
            $form_data['page'] = $_REQUEST['page'];
        }

        if ($query->have_posts()) {
            $result['found_jobs'] = true;

            while ($query->have_posts()) {
                $query->the_post();
                global $post;

                mylisting_locate_template('partials/listing-preview.php', [
                    'listing' => $post,
                    'wrap_in' => isset($args['item-wrapper']) ? $args['item-wrapper'] : 'col-md-4 col-sm-6 col-xs-12 reveal',
                ]);

                $result['data'][] = $post->_c27_marker_data;
            }
        } else {
            get_job_manager_template_part('content', 'no-jobs-found');
        }

        $result['html'] = ob_get_clean();
        $result['pagination'] = get_job_listing_pagination($query->max_num_pages, (absint(isset($form_data['page']) ? $form_data['page'] : 0) + 1));
        $result['max_num_pages'] = $query->max_num_pages;
        $result['found_posts'] = $query->found_posts;
        $result['formatted_count'] = number_format_i18n($query->found_posts);

        if ($query->found_posts < 1) {
            $result['showing'] = __('No results', 'my-listing');
        } elseif ($query->found_posts == 1) {
            $result['showing'] = __('One result', 'my-listing');
        } else {
            $result['showing'] = sprintf(__('%d results', 'my-listing'), $query->found_posts);
        }

        wp_send_json($result);
    }

    public function query($args = [])
    {
        global $wpdb;

        add_filter('posts_join', [$this, 'priority_field_join'], 30, 2);
        add_filter('posts_orderby', [$this, 'priority_field_orderby'], 30, 2);
        add_filter('posts_distinct', [$this, 'prevent_duplicates'], 30, 2);

        $args = wp_parse_args($args, array(
            'search_location' => '',
            'search_keywords' => '',
            'offset' => 0,
            'posts_per_page' => 20,
            'orderby' => 'date',
            'order' => 'DESC',
            'fields' => 'all',
            'post__in' => [],
            'post__not_in' => [],
            'meta_query' => [],
            'tax_query' => [],
            'author' => null,
            'ignore_sticky_posts' => true,
            'mylisting_orderby_rating' => false,
            'mylisting_ignore_priority' => false,
        ));

        do_action('get_job_listings_init', $args);

        $query_args = array(
            'post_type' => 'job_listing',
            'post_status' => 'publish',
            'ignore_sticky_posts' => $args['ignore_sticky_posts'],
            'offset' => absint($args['offset']),
            'posts_per_page' => intval($args['posts_per_page']),
            'orderby' => $args['orderby'],
            'order' => $args['order'],
            'tax_query' => $args['tax_query'],
            'meta_query' => $args['meta_query'],
            'update_post_term_cache' => false,
            'update_post_meta_cache' => false,
            'cache_results' => false,
            'fields' => $args['fields'],
            'author' => $args['author'],
            'mylisting_orderby_rating' => false,
            'mylisting_ignore_priority' => false,
            'mylisting_prevent_duplicates' => true,
        );


        // WPML workaround
        if ((strstr($_SERVER['REQUEST_URI'], '/jm-ajax/') || !empty($_GET['jm-ajax'])) && isset($_POST['lang'])) {
            do_action('wpml_switch_language', sanitize_text_field($_POST['lang']));
        }

        if ($args['posts_per_page'] < 0) {
            $query_args['no_found_rows'] = true;
        }

        if (!empty($args['search_location'])) {
            $query_args['meta_query'][] = [
                'key' => '_job_location',
                'value' => $args['search_location'],
                'compare' => 'LIKE'
            ];
        }

        if (!empty($args['post__in'])) {
            $query_args['post__in'] = $args['post__in'];
        }

        if (!empty($args['post__not_in'])) {
            $query_args['post__not_in'] = $args['post__not_in'];
        }

        if (!empty($args['search_keywords'])) {
            $query_args['s'] = $GLOBALS['mylisting_search_keywords'] = sanitize_text_field($args['search_keywords']);
            add_filter('posts_search', [$this, 'keyword_search']);
        }

        $query_args = apply_filters('job_manager_get_listings', $query_args, $args);

        if (empty($query_args['meta_query'])) {
            unset($query_args['meta_query']);
        }

        if (empty($query_args['tax_query'])) {
            unset($query_args['tax_query']);
        }

        if (!$query_args['author']) {
            unset($query_args['author']);
        }

        /** This filter is documented in wp-job-manager.php */
        $query_args['lang'] = apply_filters('wpjm_lang', null);

        // Filter args
        $query_args = apply_filters('get_job_listings_query_args', $query_args, $args);
        $query_args = apply_filters('mylisting/explore/args', $query_args, $args);

        // Generate hash
        $to_hash = json_encode($query_args) . apply_filters('wpml_current_language', '');
        $query_args_hash = 'jm_' . md5($to_hash) . WP_Job_Manager_Cache_Helper::get_transient_version('get_job_listings');

        do_action('before_get_job_listings', $query_args, $args);

        // Cache results
        if (apply_filters('get_job_listings_cache_results', true)) {
            if (false === ($result = get_transient($query_args_hash))) {
                $result = new WP_Query($query_args);
                set_transient($query_args_hash, $result, DAY_IN_SECONDS * 30);
            }

            // random order is cached so shuffle them
            if ($query_args['orderby'] == 'rand') {
                shuffle($result->posts);
            }
        } else {
            $result = new WP_Query($query_args);
        }

        do_action('after_get_job_listings', $query_args, $args);

        remove_filter('posts_join', [$this, 'priority_field_join'], 30);
        remove_filter('posts_orderby', [$this, 'priority_field_orderby'], 30);
        remove_filter('posts_distinct', [$this, 'prevent_duplicates'], 30);

        // Remove rating field filter if used.
        remove_filter('posts_join', [$this, 'rating_field_join'], 35);
        remove_filter('posts_orderby', [$this, 'rating_field_orderby'], 35);

        return $result;
    }

    public function query_for_quick_search($args = [])
    {
        global $wpdb;

        add_filter('posts_join', [$this, 'priority_field_join'], 30, 2);
        add_filter('posts_orderby', [$this, 'priority_field_orderby'], 30, 2);
        add_filter('posts_distinct', [$this, 'prevent_duplicates'], 30, 2);

        $args = wp_parse_args($args, array(
            'offset' => 0,
            'posts_per_page' => 20,
            'fields' => 'all',
            'post__in' => [],
            'mylisting_orderby_rating' => false,
            'mylisting_ignore_priority' => false,
        ));

        do_action('get_job_listings_init', $args);

//        $query_args = array(
//            'post_type' => 'job_listing',
//            'post_status' => 'publish',
//            'ignore_sticky_posts' => $args['ignore_sticky_posts'],
//            'offset' => absint($args['offset']),
//            'posts_per_page' => intval($args['posts_per_page']),
//            'orderby' => $args['orderby'],
//            'order' => $args['order'],
//            'tax_query' => $args['tax_query'],
//            'meta_query' => $args['meta_query'],
//            'update_post_term_cache' => false,
//            'update_post_meta_cache' => false,
//            'cache_results' => false,
//            'fields' => $args['fields'],
//            'author' => $args['author'],
//            'mylisting_orderby_rating' => false,
//            'mylisting_ignore_priority' => false,
//            'mylisting_prevent_duplicates' => true,
//            'post__in' => [245492, 228539, 198757, 198724, 198714, 198698, 164279, 125080, 125072, 125057, 125046, 125019, 124996, 124988, 124981, 124971, 124953, 122152, 122137, 122128, 122121, 122113, 121991, 94771, 94627, 94613, 94574, 94572, 94563, 94559, 94539, 94532, 94526, 94517, 94511, 94505, 94105, 94093, 94086, 94079, 93843, 93836, 93825, 93781, 93768, 93761, 80343, 80202, 80181, 16783, 16774, 16767, 16758, 16751, 16726, 16714, 16703, 16695, 16680, 1948, 16496, 16487, 16480, 16468, 16457, 16450, 16441, 16196, 16101, 16091, 16084, 16076, 16060, 16056, 15830, 5541, 2094, 2088, 2079, 2073, 2063, 2042, 2040, 2035, 2000, 1995, 1994, 1988, 1982, 1969, 1968, 1966, 1964, 1946, 1945, 1943, 1940, 1937, 1930, 1929, 1926, 1920, 1882, 1873, 1871, 1869, 1866, 1860, 1858, 1856, 1849, 1835, 1828, 1821, 1786, 1778, 1777, 1776, 1773, 1770, 1769, 5540, 1711, 1689, 1684, 1660, 1655, 1559, 1557, 1554, 1553, 1546, 1542, 1541, 1538, 1537, 1527, 1521, 1519, 1510, 1194, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1154, 1155, 1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1170, 1171, 1173, 5539, 1134, 1135, 1136, 1137, 1138, 1139, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1151, 1152, 1153, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132, 1133, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1089, 1090, 1091, 1092, 1093, 1054, 1055, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1066, 1067, 1068, 1069, 1070, 1071, 1073, 5538, 1034, 1035, 1036, 1037, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 1024, 1025, 1026, 1027, 1028, 1030, 1031, 1032, 1033, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 5537, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 684, 974, 975, 976, 977, 978, 979, 980, 981, 982, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 677, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 969, 970, 971, 972, 973, 878, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 916, 917, 918, 919, 920, 921, 922, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 896, 897, 898, 899, 900, 901, 902, 903, 904, 906, 907, 908, 909, 910, 911, 912, 913, 914, 876, 877, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 856, 857, 858, 859, 860, 861, 862, 863, 864, 866, 867, 868, 869, 870, 871, 872, 873, 874, 836, 837, 838, 839, 841, 843, 844, 845, 846, 847, 848, 849, 850, 851, 853, 854, 855, 5536, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 830, 831, 832, 833, 834, 835, 5535, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 776, 777, 778, 779, 780, 781, 782, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 768, 769, 770, 771, 772, 773, 774, 775, 280, 756, 757, 758, 759, 760, 761, 762, 765, 766, 767, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 690, 697, 698, 699, 700, 701, 702, 703, 704, 705, 707, 709, 710, 711, 712, 713, 714, 715, 685, 683, 676, 673, 672, 671, 670, 668, 663, 660, 658, 656, 655, 653, 647, 646, 645, 644, 643, 642, 641, 640, 639, 638, 637, 636, 634, 633, 632, 631, 630, 629, 628, 627, 626, 625, 624, 623, 622, 621, 620, 619, 617, 611, 605, 603, 601, 600, 594, 592, 590, 586, 576, 573, 572, 569, 568, 562, 561, 560, 559, 558, 557, 305, 300, 296, 289, 235, 1512, 245404, 245390, 117297, 116858, 94749, 94736, 94719, 94704, 94700, 94698, 94696, 94684, 80530, 80536, 80541, 80533, 80531, 80528, 80526, 80523, 80521, 80519, 80516, 80514, 80512, 80510, 80508, 80506, 80504, 80502, 80500, 80496, 80492, 80490, 80478, 80240, 80228, 16979, 15891, 15884, 15878, 15869, 15866, 15854, 15844, 15835, 15780, 15772, 15761, 15756, 15748, 15742, 15736, 15730, 15658, 15650, 15642, 15634, 15629, 15619, 15600, 15595, 15587, 15579, 15533, 15515, 15439, 15364, 15091, 15073, 15057, 15041, 14985, 14887, 14877, 6906, 6825, 6807, 6784, 6778, 6774, 6766, 6747, 6743, 6729, 6713, 6704, 6698, 6683, 6651, 6645, 6637, 5660, 5657, 5644, 5638, 5609, 5601, 5594]
//        );


        /*
        * JETPACK SEARCH
        */


        //   if (class_exists('Jetpack_Search')) { 	// fallback to regular if jetpack search is not available

        $jet_result = improved_widget_search_v4(sanitize_text_field($args['jet_search']));


        if (!empty($jet_result)) {          //  fallback to regular search if jetpack returns no results

            unset($query_args);


            $query_args = array(
	            'post_type' => 'job_listing',
	            'post_status' => 'publish',
                'post__in' => $jet_result,  //works
	            'fields' => $args['fields'],
	            //  'post__in' => improved_widget_search_v4('solo'), //works
                // 'post__in' =>  $filter,
              //  'post_status' => 'publish',
              //  'post_type' => 'any',


            );
        }

        //  }


        /* END JETPACK */


        // WPML workaround
        if ((strstr($_SERVER['REQUEST_URI'], '/jm-ajax/') || !empty($_GET['jm-ajax'])) && isset($_POST['lang'])) {
            do_action('wpml_switch_language', sanitize_text_field($_POST['lang']));
        }

        if ($args['posts_per_page'] < 0) {
            $query_args['no_found_rows'] = true;
        }

//        if (!empty($args['post__in'])) {
//            $query_args['post__in'] = $args['post__in'];
//        }

//        if (!empty($args['post__not_in'])) {
//            $query_args['post__not_in'] = $args['post__not_in'];
//        }

//        if (!empty($args['search_keywords'])) {
//
//            $query_args['s'] = $GLOBALS['mylisting_search_keywords'] = sanitize_text_field($args['search_keywords']);
//            add_filter('posts_search', [$this, 'keyword_search']);
//        }

        $query_args = apply_filters('job_manager_get_listings', $query_args, $args);

//        if (empty($query_args['meta_query'])) {
//            unset($query_args['meta_query']);
//        }
//
//        if (empty($query_args['tax_query'])) {
//            unset($query_args['tax_query']);
//        }
//
//        if (!$query_args['author']) {
//            unset($query_args['author']);
//        }

        /** This filter is documented in wp-job-manager.php */
//        $query_args['lang'] = apply_filters('wpjm_lang', null);

        // Filter args
//        $query_args = apply_filters('get_job_listings_query_args', $query_args, $args);
//        $query_args = apply_filters('mylisting/explore/args', $query_args, $args);

        // Generate hash
        $to_hash = json_encode($query_args) . apply_filters('wpml_current_language', '');
        $query_args_hash = 'jm_' . md5($to_hash) . WP_Job_Manager_Cache_Helper::get_transient_version('get_job_listings');

//        do_action('before_get_job_listings', $query_args, $args);

        // Cache results
        if (apply_filters('get_job_listings_cache_results', true)) {
            if (false === ($result = get_transient($query_args_hash))) {
                $result = new WP_Query($query_args);
                set_transient($query_args_hash, $result, DAY_IN_SECONDS * 30);
            }

            // random order is cached so shuffle them
            if ($query_args['orderby'] == 'rand') {
                shuffle($result->posts);
            }
        } else {
            $result = new WP_Query($query_args);
        }



        do_action('after_get_job_listings', $query_args, $args);

        remove_filter('posts_join', [$this, 'priority_field_join'], 30);
        remove_filter('posts_orderby', [$this, 'priority_field_orderby'], 30);
        remove_filter('posts_distinct', [$this, 'prevent_duplicates'], 30);

        // Remove rating field filter if used.
        remove_filter('posts_join', [$this, 'rating_field_join'], 35);
        remove_filter('posts_orderby', [$this, 'rating_field_orderby'], 35);

//		$result['request'] = "
//		  ORDER BY  CAST( COALESCE( priority_meta.meta_value, 0 ) AS UNSIGNED ) DESC
//		";
        return $result;


    }

    /**
     * To order listings by priority, we need to use a LEFT JOIN in wp_postmeta
     * instead of an INNER JOIN, so we can fetch listings that don't have the field
     * set at all, and the use COALESCE to provide a default value of 0.
     *
     * @since 1.7.0
     */
    public function priority_field_join($join, $query)
    {
        // Ignore order by priority if 'mylisting_ignore_priority' query var is set.
        if (!empty($query->query_vars['mylisting_ignore_priority'])) {
            return $join;
        }

        global $wpdb;
        $join .= "
	        LEFT JOIN {$wpdb->postmeta} as priority_meta ON(
	            {$wpdb->posts}.ID = priority_meta.post_id AND priority_meta.meta_key = '_featured'
	        ) ";

        return $join;
    }

    /**
     * Order listings by priority first, then other clauses.
     *
     * @since 1.7.0
     */
    public function priority_field_orderby($orderby, $query)
    {
        // Ignore order by priority if 'mylisting_ignore_priority' query var is set.
        if (!empty($query->query_vars['mylisting_ignore_priority'])) {
            return $orderby;
        }

        // Order by listing priority, defaults to zero if meta_value is null.
        $order = " CAST( COALESCE( priority_meta.meta_value, 0 ) AS UNSIGNED ) DESC ";

        // Include any other order by clauses, with lower priority.
        if (trim($orderby)) {
            $order .= ", $orderby ";
        }

        return $order;
    }

    /**
     * Add rating left join clause. Similar to priority_field_join.
     *
     * @since 1.7.0
     */
    public function rating_field_join($join, $query)
    {
        global $wpdb;
        $join .= "
	        LEFT JOIN {$wpdb->postmeta} as rating_meta ON(
	            {$wpdb->posts}.ID = rating_meta.post_id AND rating_meta.meta_key = '_case27_average_rating'
	        ) ";
        return $join;
    }

    /**
     * Add order by rating clause. Similar to priority_field_orderby.
     *
     * @since 1.7.0
     */
    public function rating_field_orderby($orderby, $query)
    {
        global $wpdb;

        $order = " CAST( COALESCE( rating_meta.meta_value, 0 ) AS DECIMAL(10, 2) ) DESC ";
        if (trim($orderby)) {
            $order .= ", $orderby ";
        }

        // Prevent duplicate results
        // @see https://helpdesk.27collective.net/questions/question/some-listings-missings-from-explore-page/
        $order .= ", {$wpdb->posts}.post_date DESC";

        return $order;
    }

    /**
     * Prevent duplicate results in listings query.
     *
     * @since 1.7.1
     */
    public function prevent_duplicates($distinct, $query)
    {
        return 'DISTINCT';
    }

    /**
     * Modified version of `get_job_listings_keyword_search` method.
     * Adds join and where query for keywords.
     *
     * @since 2.0.4
     */
    function keyword_search($search)
    {
        global $wpdb;
        return $search;

        // current search keyword
        $keyword = $GLOBALS['mylisting_search_keywords'];

        // Set Search DB Conditions.
        $conditions = [];

        // search meta
        $searchable_meta_keys = apply_filters('mylisting/explore/keyword-search/meta-fields', [
            '_job_location',
            '_job_tagline',
        ]);

        if (!empty($searchable_meta_keys)) {
            $conditions[] = "{$wpdb->posts}.ID IN ( SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key IN ( '" . implode("','", array_map('esc_sql', $searchable_meta_keys)) . "' ) AND meta_value LIKE '%" . esc_sql($keyword) . "%' )";
        }

        // search taxonomies
        if (apply_filters('mylisting/explore/keyword-search/include-taxonomies', true) !== false) {
            $conditions[] = "{$wpdb->posts}.ID IN ( SELECT object_id FROM {$wpdb->term_relationships} AS tr LEFT JOIN {$wpdb->term_taxonomy} AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id LEFT JOIN {$wpdb->terms} AS t ON tt.term_id = t.term_id WHERE t.name LIKE '%" . esc_sql($keyword) . "%' )";
        }

        if (empty($conditions)) {
            return $search;
        }

        $conditions_str = implode(' OR ', $conditions);

        if (!empty($search)) {
            $search = preg_replace('/^ AND /', '', $search);
            $search = " AND ( {$search} OR ( {$conditions_str} ) )";
        } else {
            $search = " AND ( {$conditions_str} )";
        }

        return $search;
    }
}
