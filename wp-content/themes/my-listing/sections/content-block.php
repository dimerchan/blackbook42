<?php
	$data = c27()->merge_options([
			'icon' => '',
			'icon_style' => 1,
			'title' => '',
			'content' => '',
			'wrapper_class' => 'block-element grid-item reveal',
			'wrapper_id' => '',
			'ref' => '',
			'escape_html' => true,
			'allow-shortcodes' => false,
			'allow-embeds' => true,
		], $data);

	if ( is_array( $data['content'] ) ) {
		$data['content'] = join( ', ', $data['content'] );
	}

	if ( $data['allow-shortcodes'] ) {
		if ( ! empty( $GLOBALS['wp_embed'] ) ) {
			$data['content'] = $GLOBALS['wp_embed']->autoembed( $data['content'] );
		}

//		$data['content'] = do_shortcode( $data['content'] );
	}
?>

<?php
global $ca_blocks_output_start;
$ca_blocks_output_start = false;
$title = $data['title'];
$handled_content_blocks = ['Medical Team', 'Fitness & Beauty Team', 'Hangouts', 'Products & Procedures', 'Celebrities', 'Featured Venues', 'Featured in', 'Brands'];
if((in_array($title, $handled_content_blocks)) && strlen($data['content']) > 10):
    if($title === 'Products & Procedures') {
        preg_match_all('/id=(\d+)/',$data['content'],$m);
        if(count($m[1]) > 0)
            $data['content'] = "[product id=" . implode(",", $m[1]) . "]";
//        echo "<script>alert('" . $data['content'] . "')</script>";
    }
    ?>
    <?php if($ca_blocks_output_start === false): ?>
        <div class="separator"></div>
    <?php endif; ?>
    <?php $ca_blocks_output_start = true; ?>
    <div style="clear: both; min-width:900px!important;"></div>
    <h3 class="col-sm-12"style="text-align: left!important; width: 100%; height: 72px!important; margin: auto!important; vertical-align: middle;"><?php echo esc_html( $data['title'] ) ?></h3>
        <div class="container c27-related-listings-wrapper">
            <div class="row listings-loading tab-loader" style="display: none;">
                <div class="loader-bg">
                    <div class="paper-spinner center-vh" style="width: 28px; height: 28px;">
                        <div class="spinner-container active">
                            <div class="spinner-layer layer-1" style="border-color: #777;">
                                <div class="circle-clipper left">
                                    <div class="circle" style="border-width: 3px;"></div>
                                </div><div class="gap-patch">
                                    <div class="circle" style="border-width: 3px;"></div>
                                </div><div class="circle-clipper right">
                                    <div class="circle" style="border-width: 3px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>        </div>
            </div>
            <div class="row section-body i-section">
                <div class="c27-related-listings tab-contents" style="display: block;">

                    <?php echo do_shortcode($data['content']) ?>


                </div>
            </div>
            <div class="row">
                <div class="c27-related-listings-pagination tab-pagination" style="display: block;"></div>
            </div>
        </div>
<?php else: ?>

<div class="<?php echo esc_attr( $data['wrapper_class'] ) ?>" <?php echo $data['wrapper_id'] ? sprintf( 'id="%s"', $data['wrapper_id'] ) : '' ?>>
	<div class="element content-block <?php echo esc_attr( $data['escape_html'] ) ? 'plain-text-content' : 'wp-editor-content' ?>">
		<div class="pf-head">
			<div class="title-style-1 title-style-<?php echo esc_attr( $data['icon_style'] ) ?>">
				<?php if ($data['icon_style'] != 3): ?>
					<?php echo c27()->get_icon_markup($data['icon']) ?>
				<?php endif ?>
				<h5><?php echo esc_html( $data['title'] ) ?></h5>
			</div>
		</div>
		<div class="pf-body">
			<p>
				<?php if ($data['escape_html']): ?>
					<?php echo wp_kses( nl2br( $data['content'] ), ['br' => []] ) ?>
				<?php else: ?>
					<?php echo wpautop( do_shortcode($data['content']) ) ?>
				<?php endif ?>
			</p>
		</div>
	</div>
</div>
<?php endif ?>