<?php

###############################
#все классы которые будут использоватся в проекте
#но пока только Mysql
###############################


class StartMysql {//для запуска использовать $db = StartMysql::connect();
    
    
   public static function connect($config = [
            'mysgl_host' => 'ca.ca8m86quluo4.eu-west-1.rds.amazonaws.com:3306',
            'mysgl_user' => 'ca',
            'mysgl_pass' => '1Love2love',
            'mysgl_base' => 'ca'
        ]) 
            {
        
        //ЛОГИРУЕМ ЗАПРОС
        //file_put_contents(ROOT . '/logs/sqlLog' . date('Ymd') . '.txt', date("Y-m-d H:i:s") . "; Connecting".PHP_EOL, FILE_APPEND);
        
        $db = new mysqli($config['mysgl_host'], $config['mysgl_user'], $config['mysgl_pass'], $config['mysgl_base'])
                or die('не подключился' . mysqli_error($db));
        return $db;
    }
    
    public static function query($db,$sql){
        //подключаемся к базе, если нет готового подключения
        if(!$db) $db = StartMysql::connect ();
        
        mysqli_query($db,"SET NAMES 'UTF-8'");
        
        //ЛОГИРУЕМ ЗАПРОС
        //file_put_contents(ROOT . '/logs/sqlLog' . date('Ymd') . '.txt', date("Y-m-d H:i:s") . "; $sql".PHP_EOL, FILE_APPEND);
        
        //проверяем мультизапрос или нет
        if (!preg_match('/;/', $sql)){
            
            $r = mysqli_query($db,$sql);
            if (!$r) return mysqli_error($db);
            if (!is_bool($r)){
                
                $res = [];
                
                while ($row = $r->fetch_array(MYSQLI_ASSOC)){
                    
                    $res[] = $row;
                    
                }
                
               // $res = $r->fetch_all(MYSQLI_ASSOC);
                $r->free();
                return $res;
            } else {
                return $r;
            }
            
            
        } else {
            
            if ($db->multi_query($sql)) {
                
               
                do{
                    
                    /* получаем первый результирующий набор */
                    if ($r = $db->store_result()) {
                     
                        $rows = [];

                        while ($row = $r->fetch_array(MYSQLI_ASSOC)){

                            $rows[] = $row;

                        }
                        $res[] = $rows;

                    }
                    
                    
                } while ($db->more_results() && $db->next_result());
                
                if (!is_bool($r))  {$r->free();}
                
                if (isset($res)) {return $res;}
                
                
            } else {
                
            return mysqli_error($db);
                        
            }
            
        }
        
    }

}

