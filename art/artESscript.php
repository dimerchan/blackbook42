<?php

namespace Elasticsearch;

use StartMysql;

/**
 * Description of artESscript
 * this class will work with elastic search instances
 *
 * @author user
 */
class artESscript {
    
    public static $host = ['https://vpc-blackbook42-g47ltxbfv6c2prnby267oals2m.eu-west-1.es.amazonaws.com:443'];
    
    public static $index = 'blackbook42';
    
    private $db;
    
    private $client;
    
    private $post_id;
    
    private $currentPost;
    
    private $searchQuery;
    
    public function __construct() {
        
        $this->db = StartMysql::connect();
        
        $this->buildESClient();
        
    }
    
    /**
     * 
     * this method will perform search 
     * actions and output search results 
     * with HTML markup
     * 
     * @param string $text - searching text
     */
    public function seach(string $text) {
        
        $this->searchQuery = $this->sanitizeQuery($text);
        
        $query_string = [

                'query' => "$this->searchQuery*",

                'fields' => [
                    'post_title',
                    'post_content',
                    ],

        ];
        
        $params = [
            'index' => 'blackbook42',
            'body' => [
                'query' => [
                    'bool' =>[
                        'must' => [
                            [
                            "query_string" => $query_string
                            ]
                        ]
                    ]
                ],
                
                'size' => 5,//size
            ]
        ];
        
        $this->performSearch($params);
        
    }
    
    private function sanitizeQuery($text) {
        
        return $this->db->real_escape_string($text);
        
    }
    
    private function performSearch($params){
        
        try{

            $response = $this->client->search($params);

            $hits = $response['hits']['hits'];

            $count = $response['hits']['total']['value'];
            
            unset($response);
            
        } catch (Exception $ex) {

            $this->logError($ex->getMessage());

            exit();

        }
        
        $this->createResponseObject($hits);
        
    }
    
    private function createResponseObject($hits) {
        
        $sorted = $this->sortResults($hits);
        
        $content = '';
        
        foreach ($sorted as $type => $listings) {
            
            $content .= "<li class='ir-cat'>$type</li>";
            
            foreach ($listings as  $listing) {
                
                $logo = $listing['_job_logo'] ?? 'https://celebrityapproved.co/wp-content/themes/my-listing/assets/images/marker.jpg';
                
                $content .= "<li><a href='$listing[guid]'>"
                        . "<div class='avatar'><img src='$logo'></div>"
                        . "<span class='category-name'>$listing[post_title]</span></a></li>";
                
                
            }
            
        }
        
        $response = (object)[
            'content' => $content
        ];
        
        echo json_encode($response);
        
    }
    
    
    private function sortResults($hits){
        
        
        $sorted = [];
        
        //sort by type
        foreach ($hits as  $hit) {
            
            $source = $hit['_source'];
            
            if (!isset($source['_case27_listing_type'])){
                $source['_case27_listing_type'] = 'UNKNOWN';
            }
            
            if (isset($sorted[$source['_case27_listing_type']])){
                
                $sorted[$source['_case27_listing_type']][] = $source;
                
            } else {
                
                $sorted[$source['_case27_listing_type']] = [];
                
                $sorted[$source['_case27_listing_type']][] = $source;
                
            }
            
        }
        
        return $sorted;
        
    }
    
    private function buildESClient(){
    
        $this->client = ClientBuilder::create()
                    ->setHosts(self::$host) 
                    ->build();
        
    }
    /**
     * this method will update Elastic search index
     * If you need to update single post need to set ID of the post
     * If $post_id is set to 0 script will update all posts
     * 
     * This method should be called on each listing update or listing insert
     * 
     * @param int $post_id = postid from DB or 0 if update all
     */
    public function updateIndex(int $post_id) {
        
        if ($post_id > 0){

            $this->post_id = $post_id;

            $this->getPost();

            $this->getPostMeta();
            
            $this->indexCurrentPost();
            
        }
        
        
        if ( $post_id === 0){
            
            $this->updateAllPosts();
            
        }

    }
    
    private function getPost() {
        
                
        $sql = "SELECT ID,post_title,post_content,post_status,guid FROM wp_posts WHERE post_type LIKE 'job_listing' AND ID = $this->post_id ORDER BY ID DESC LIMIT 1";

        $post = StartMysql::query($this->db, $sql);
        
        $this->checkMysqlError();
        
        if (!empty($post[0])
                && is_array($post[0])){

            $this->currentPost = $post[0];
            
        } else {
            
            exit("Error on ID $this->post_id, post not found");
            
        }
        
        
    }
        
    private function updateAllPosts() {
        
        $sql = "SELECT ID FROM wp_posts WHERE post_type LIKE 'job_listing'";
        
        $allIDS = StartMysql::query($this->db, $sql);
        
        $this->checkMysqlError();
        
        foreach ($allIDS as $key => $value) {
            
            $this->post_id = $value['ID'];

            $this->getPost();

            $this->getPostMeta();

            $this->indexCurrentPost();
            
            unset($allIDS[$key]);
        
            
        }

    }

    private function getPostMeta() {

        $sql = 'SELECT meta_key,meta_value FROM wp_postmeta WHERE post_id  = ' 
                . $this->currentPost['ID'] . ' AND (meta_key LIKE "_case27_listing_type" OR meta_key LIKE "_job_logo")';

        $meta = StartMysql::query($this->db, $sql);

        //TODO:ADD CHECK OF DATA


        if (!empty($meta)
                && is_array($meta)){


            foreach ($meta as $metaRecord) {

                      $this->currentPost[$metaRecord['meta_key']] = $metaRecord['meta_value'];

            }
        }
    }
    
    private function indexCurrentPost() {
        
            if (empty($this->currentPost)){
                
                $this->logError("Empty Current post on ID $this->post_id");
                
                return;
                
            }
            
            if ($this->currentPost['post_status'] == 'publish'){

                try {

                    $params = [
                        'index' => 'blackbook42',
                        'id'    => $this->currentPost['ID'],
                        'body'  => $this->currentPost
                    ];

                    $response = $this->client->index($params);

                } catch (Exception $er){

                    print_r($er->getMessage());

                    exit();
                }

                //print_r($response);
                print_r("\nINDEXED $this->post_id\n");
                
            } else {
                
                if ($this->documentExists()){
                    
                    $params = [
                        'index' => 'blackbook42',
                        'id'    => $this->currentPost['ID']
                    ];

                    $this->client->delete($params);
                    
                    //print_r($response);
                    print_r("\nDELETED $this->post_id\n");

                }
            }
            
            $this->clearCurrentPost();
            
            
    }
    
    private function documentExists() {
        
        try{

            $params = [
                'index' => 'blackbook42',
                'id'    => $this->currentPost['ID']
            ];

            $this->client->get($params);
            
            return true;

        } catch (Common\Exceptions\Missing404Exception $er){

            return false;
            
        }
        
        
    }
    
    
    private function clearCurrentPost(){
        
        $this->post_id = null;
        
        $this->currentPost = [];
        
    }
    
    public function __destruct() {
        
        $this->db->close();
        
        unset ($this->client);
        
    }
    
    private function checkMysqlError() {
        
        if ($this->db->errno){
            
            $this->logError("Mysql Error on post_id $this->post_id, Error mess: " . $this->db->error);
            
            exit("Mysql Error, check log file\n");
            
        }
        
    }
    
    
    private function logError($text) {
        
            file_put_contents(ROOT . '/error.log', date('Y-m-d H:s:i') . $text .PHP_EOL, FILE_APPEND);
            
    }
    
}
