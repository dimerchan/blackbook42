<?php

/* 
 * this module will upload document into Elastic search index...
 * if id is passed, selected document will be updated
 */

if (!defined('ROOT')) define('ROOT', __DIR__);

define('POST', $_POST);
define('GET', $_GET);

require_once ROOT . '/vendor/autoload.php';
require_once ROOT . '/classes.php';
require_once ROOT . '/artESscript.php';

$post_id = $argv[1] ?? '';

use Elasticsearch\artESscript;

$script = new artESscript();

$script->updateIndex($post_id);
