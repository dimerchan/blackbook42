<?php
ini_set('display_errors', 1);
$log_file = '/opt/bitnami/apache2/logs/error_log';

$file = file($log_file,FILE_IGNORE_NEW_LINES);

if (isset($_GET['truncate'])){
    
    passthru('truncate -s 0 /opt/bitnami/apache2/logs/error_log');
    
    $newUrl = explode('?', $_SERVER['REQUEST_URI']);
    
    header('location:' . $newUrl[0] );
    
    exit();
}

?><!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Error viewer "<?php echo $log_file?>"</title>
        <style>
            table{
                max-width: 100%;
                
            }
            table td.errorMessage{
                word-break: break-word;
            }
            button {
                margin: 10px;
                padding: 10px;
                border-radius: 5px;
                color: #f6f9f6;
                background: #44bf44;
            }
            button#truncate {
                position: absolute;
                right: 5%;
                background: #c72828;
            }
        </style>
        <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    </head>
    <body>
        <button value="Unique Errors" id="unique">Unique Errors</button>
        <button value="Full list" id="fulllist">Full list</button>
        <button value="Refresh" id="refresh">Refresh</button>
        
        <button value="Truncate LogFile" id="truncate" onclick="truncateLogFile();">Truncate LogFile</button>
        <?php
        
                    if (empty($file)){
                        
                        ?>

                    <tr>
                        <td colspan="6" id="LogFileIsEmpty"><b>LogFile is empty. Location: <?php echo $log_file?></b></td>
                    </tr>

                        <?php
                        
                    } else {
        ?>
        <table id='errorLogTable' class="display hover order-column row-border stripe">
            <thead>
            <tr>
                <th>Date</th>
                <th>Level</th>
                <th>Proc ID</th>
                <th>Frequency</th>
                <th>Client</th>
                <th>Message</th>
            </tr>
            </thead>
            <tbody>
                <?php
                
                
                    foreach ($file as $key => $value) {

                        if (preg_match('/\[(.*)\]\s\[(.*)\]\s\[(.*)\]\s\[(.*?)\]\s(.*)/', $value,$m)){

                            $date  = preg_replace('/\.\d{6}/', '', $m[1]);
                            $level = $m[2];
                            $proc_id = $m[3];
                            $client = $m[4];
                            $message = $m[5];
                            
                            ?>

                                <tr>
                                    <td><?php echo (new DateTime($date))->format('Y-m-d H:i:s')?></td>
                                    <td><?php echo $level?></td>
                                    <td><?php echo $proc_id?></td>
                                    <td data-id="<?php echo md5($message)?>" class="frequency"><?php echo 1?></td>
                                    <td><?php echo $client?></td>
                                    <td class="errorMessage"><?php echo str_replace(['\n','\t'], ['<br>',''], $message)?></td>
                                </tr>

                            <?php
                            
                        } else {
                            echo "Incorrect string $value<br>";
                        }
                    }
                    
                ?>
            </tbody>
        </table>
        <?php } ?>
        <script>
        
        var unique = {};
        
        document.querySelectorAll('.frequency').forEach((x)=>{
            
            var id = x.getAttribute('data-id');
            
            if (unique[id]){
                
                unique[id]++;
                
            } else {
                
                unique[id] = 1;
                
            }
            
            
        })
        
        for(let n in unique){
            
            document.querySelectorAll('[data-id="' + n + '"]').forEach((x,y)=>{
                
                if (y == 0) x.classList.add('unique');
                
                x.innerText = unique[n];
                
            });
            
        }
        function truncateLogFile(){
            
            document.location.href = document.location.href + '?truncate';
            
        }
        </script>
        <script type="text/javascript" src="//code.jquery.com/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script>
            
            var uniqueRows = false;
            
            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex , rowData, counter ) {
                    
                    if (typeof(table) === 'undefined') return true;
                    
                    if (uniqueRows) {
                        if ( table.row(dataIndex).node().querySelector('.unique'))
                        {
                            return true;
                        }
                        return false;
                        
                    } else {
                        return true;
                    }
                }
            );
            if (!document.getElementById('LogFileIsEmpty')){
                
                var table;
                $(document).ready( function () {
                    table = $('#errorLogTable').DataTable();

                    var unique = document.getElementById('unique');

                    unique.addEventListener('click',()=>
                                (uniqueRows = true) && table.draw()
                            );


                    var fulllist = document.getElementById('fulllist');

                    fulllist.addEventListener('click',()=>
                                !(uniqueRows = false) && table.draw()
                            );


                    var refresh = document.getElementById('refresh');

                    refresh.addEventListener('click',()=>
                                document.location.reload()
                            );

                } );
            }
        </script>
    </body>
</html>