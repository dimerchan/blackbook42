<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
	
        $empteRequest = true;
        
        /**
         * empty search request we return empty result
         */
        foreach ($_GET as $key => $value) {
            
            if (preg_match('/^sSearch/', $key) === 1
                    && !empty($value)) {
                
                
                $empteRequest = false;
                
            }
            
        }   
        
        if ($empteRequest){
                
                $output = array(
                   "sEcho" => intval($_GET['sEcho']),
                   "iTotalRecords" => 0,
                   "iTotalDisplayRecords" => 0,
                   "aaData" => array()
                );
                
                echo json_encode( $output );
                
                exit();
        }
        
        if (!defined('ROOT')) define('ROOT', __DIR__);

        require ROOT . '/vendor/autoload.php';

        use Elasticsearch\ClientBuilder;

        $client = ClientBuilder::create()->build();

	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	$aColumns = array( 
                'com_name',
                'status',
                'com_type', 
                'com_hrb',  
                'com_address', 
                'com_city', 
                'com_zip', 
                'country', 
                'stammkapital',
                'area',  
                'geschaftsfuhrer', 
                'geschaftsfuhrerbd', 
                'geschaftsfuhrer2', 
                'geschaftsfuhrerbd2', 
                'geschaftsfuhrer3', 
                'geschaftsfuhrerbd3',
            );
        
	$aColumnsGlob = array( 
                'com_name',
//                'com_type', 
                'com_hrb',  
                //'com_address', 
                'com_city', 
                //'com_zip', 
//                'country', 
                //'area',  
                'geschaftsfuhrer', 
                'geschaftsfuhrer2', 
                'geschaftsfuhrer3',
            );
	
        $artialSearchFields = [
                'com_name',
                'com_hrb',  
                'com_address', 
//                'com_city', 
                'area', 
                'geschaftsfuhrer', 
                'geschaftsfuhrer2', 
                'geschaftsfuhrer3',
        ];
         

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */
	
	/* 
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}
        
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	/* Individual column filtering */
        
        
        $must = [[
                "query_string" => [

                        'query' => $_GET['sSearch'] . '*',

                        'fields' => $aColumnsGlob,

                ]]];
        
        
        
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
        
	{
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			
                    if (in_array($aColumns[$i], $artialSearchFields)){
                        
                        $must[] = [
                            "query_string" => [

                                    'query' => $_GET['sSearch_'.$i] . '*',

                                    'fields' => [$aColumns[$i]],

                            ]];
                        
                    } else {
                        
                        $must[] = [
                            "query_string" => [

                                    'query' => $_GET['sSearch_'.$i],

                                    'fields' => [$aColumns[$i]],

                            ]];
                    }
                    
		}
	}
        
                
//        var_dump($must);
//        exit();
        
		$params = [
                    'index' => 'hrgtool_index',
                    'body' => [
                        'query' => [
                            'bool' =>[
                                'must' => $must
                            ]
                        ],
                        'size' => $_GET['iDisplayLength'],//size
                        'from' => $_GET['iDisplayStart']//offset
                    ]
                ];
                
                
                try{
    
                    $response = $client->search($params);

                    $hits = $response['hits']['hits'];
                    
                    $iFilteredTotal = $response['hits']['total']['value'];
                    
                } catch (Exception $ex) {

                    echo $ex->getMessage();
                    
                    exit();
                    
                }
         
	
	
        
        
	$iTotal = 1000000;
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
        
            foreach ($hits as $aRow) {
                
                $aRow = $aRow['_source'];
                
                $row = array();
		
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $aColumns[$i] ];
			}
		}
                
		$output['aaData'][] = $row;
                    
            }
            
        
        
        
        
	
	echo json_encode( $output );
?>